� Polytron VCS logfile A  @      ����B    O p6wab C >Yp!D >YxE Initial revision.
F� F�i� F�  ********************************************************************************
**
**    Text File Name:  ASC-ASSETCHARGE
**    Text file for computing asset charges.  Should run automatically  T245
**    Designed for an all participant level run (when the participant entered on
**    T245 is all zeros) and for an the "Allocation" setting of "Prorated"
**    causing two passes to come through the calculator.
**
**    Inputs:
**    Variable List:
**     PL025 = product ID
**     FC700 = fund identifier V=Variable, F=Fixed,
**     PM400 = participant balance to waive charge
**     PM405 = minimum dollar asset fee
**     PM410 = maximum dollar asset fee
**     PM460 = asset charge scale #1
**     PM465 = asset charge scale #2
**     PM600 = scale #1 flag to deduct(D) or bill(B)
**     PM605 = scale #2 flag to deduct(D) or bill(B)
**     PM642 = flag to put asset charges on hold
**     PM643 = flag if two scales are they concurrent or the better of
**     PM645 = 1=Participant level  2=plan level
**     PM650 = use fixed funds in balance calculation y/n
**     PM655 = frequency of asset charges
**     PM660 = are scales Banded(B) or FNN(F)
**     PM665 = apply fee to fixed funds y/n
**
**     FC601 = set up for fund list, 1=variable, else a 0 T245 uses directly
**
**
**    Outputs:
**     PM319 = last asset charge date updated via this calculator
**     BR record
**
**    Transaction that starts this is T245, but runs automatically after the initial setup
**
**     COMPUTED VARIABLES
**     Series 200 for asset charges.  Note WK212 - WK217 predefined by Sungard, do not use
**     WK001 = predefined by Sungard to hold the balances
**     WK002 = Error flag
**     WK008 = predefined by Sungard to hold the plan level asset charge
**     WK009 = predefined by Sungard to hold the participant level asset charge
**     WK200 = cash balance
**     WK201 = rate for 1st asset charge   PM460
**     WK202 = second rate 2nd asset charge  PM465
**     WK203 = total $ to deduct based upon asset charge 1  PM460
**     WK204 = total $ to deduct based upon asset charge 2  PM465
**     WK205 = plan level assets
**     WK207 = Total Billed fee
**     WK209 = next date to set up asset charges
**     WK211 = flag =1 if the total plan assets have already been sent back via WK001
**     WK212 - WK220 PREDEFINED DO NOT USE
**     WK222 = if FNN need rate from B table
**     WK223 = if FNN need breakpoint for B table
**     WK225 = Trade or start date
**     WK319 = Next Propagation Date
**     WK400 = PM400
**     WK405 = PM405
**     WK410 = PM410
**     TX002 = bill/deduct flag bast on better of  PM460 use PM600     for PM465 use PM605
**     TX003 = PM645
**     TX004 = PM650
**     TX005 = PM660
**     TX006 = full name of asset charge scale using PM460 & PM660
**     TX007 = PM642
**     TX008 = full name of asset charge scale using PM465 & PM660
**     TX009 = FIRST PASS / SECOND indicator
**     TX010 = Hold first participant so know when first pass is complete
**     TX011 = Hold plan for verification that not on the next plan
**     TX018   Std Usage Code 1
**     TX020 = Hold last participant during first pass.
**
**  Note: This calculator is called for each participant, so no by participant loop needed.
**        Two passes are made. It goes through all participants once then does it again.
**
**    Created by:  Suzann Schiewer         05/19/2000
**
**    Modification History:
**    Programmer      DATE       CC    Reason for Change
**    Suzann Schiewer 02/12/01  3319   Change CABILL to CAEXP, Deduct option to D, and Bill reason
**    SPS             07/03/01  3531   change control - 1063 add XX like LN logic
**    Rick Sica       09/03/04  6187   Asset charge not being calculated correctly
**    Rick Sica       09/14/05  8111   One-time Waive of fee
**    Gary Pieratt    01/19/06  9675   Use trade date instead of run date
**    Judy Stewart    03/01/06  9709   Omniplus 5.5 Upgrade.
**    Rick Sica       03/24/06  10044  Reverse FNN (F1/F2) formula
**    Rick Sica       08/29/06  10617  901 records read/write differently for 5.5
********************************************************************************
SD080=10000;
WK001=0;WK008=0;WK009=0;
WK025=200;         /* Although this is on the Custom base tab, since it comes after the routines below, it is never hit */
IF PL011<>PH006;   /* On end of the first pass, the PH record as changed to the first part. of next plan */
  IF TX003='2';                                     /* Plan level asset charge */
    IF WK205>0;
      WK200=WK205;
      PERFORM 'CALC.ASC';
      RPLOG_INFO(FMT1:'DATE' FMT2:'CASH' AMT1:WK225 AMT2:WK200 MSG:'Plan Assets');
      IF TX002='D';                                 /* Deduct */
        WK001=WK200;
        WK008=WK203;                                /* Total fee to be prorated to all participants */
      ELSE;                                         /* Billed */
        WK207=WK203;
      END;
    ELSE;
      RPLOG_WARN(MSG:('Plan assets is zero; no fees applied.'));
    END;
  END;
  QUIT;
END;
WK002=OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:'STARTASC');
IF WK002=1;
  TX011=OCDATA_DE(DENUM:500);                       /* Get plan number of last plan through */
ELSE;
  TX011=' ';
END;
********************************************************************************
IF PL011<>TX011;                                    /* If new plan ... */
  OCDATA_CODESTROY(NAME:'ASC.T245.container');      /* start a fresh container */
  OCDATA_COCREATE(NAME:'ASC.T245.container' KEYLENG:9 TX10S:1 NUMS:2);
  WK002=PMPMOBJ_GET(PLAN:SD001 PRODUCTID:PL025);
  IF WK002=0;
    TX007='Y';                                      /* Stop further passes for plan */
    RPLOG_ERROR(MSG:'ERROR READING PM RECORD');
    QUIT;
  ELSE;
    TX007=PMPMOBJ_DE(DENUM:642);          /* The following data elements are not set but here and are held throughout the plan */
  END;
  IF TX007="N";                           /* Asset charges NOT on hold */
    WK400=PMPMOBJ_NUMDE(DENUM:400);
    IF WK400<=0;
      RPLOG_WARN(MSG:('Asset charge waive fee (PM400) set to 0; no fees will be calculated.'));
      TX007='Y';
    END;
    WK405=PMPMOBJ_NUMDE(DENUM:405);
    WK410=PMPMOBJ_NUMDE(DENUM:410);
    IF WK410<=0;
      RPLOG_WARN(MSG:('Asset charge maximum (PM410) set to 0; assuming unlimited.'));
      WK410=9999999.99;
    END;
    TX003=PMPMOBJ_DE(DENUM:645);
    IF TX003=' ';
      RPLOG_WARN(MSG:('Asset Chrg Acct Bal Calc Level (PM645) option is not set; Plan level assumed.'));
      TX003='2';
    END;
    TX004=PMPMOBJ_DE(DENUM:650);
    IF TX004=' ';
      RPLOG_WARN(MSG:('Asset Chrg Fixed Fund in Bal Calc (PM650) option is not set; Fixed funds included.'));
      TX004='Y';
    END;
    TX005=PMPMOBJ_DE(DENUM:660);
    IF TX005=' ';
      RPLOG_WARN(MSG:('Asset Charge FNN/Banded (PM660) option is not set.'));
      TX007='Y';
    END;
    TX006=PMPMOBJ_DE(DENUM:460);
    IF TX006<>' ';
      TX006="CS"+OCSUB(TX006 1 4)+TX005;  /* For first scale in PM460; See Footnote #1 */
    ELSE;
      RPLOG_WARN(MSG:('Asset Charge 1 scale not set (PM460)'));
      TX007='Y';
    END;
    TX008=PMPMOBJ_DE(DENUM:465);
    IF TX008<>' ';
      TX008="CS"+OCSUB(TX008 1 4)+TX005;             /* See Footnote #3 */
    END;
    WK225=SD206;                                     /* current trade date */
    WK002=VTTHOBJ_GET(PLAN:SD001 FILEID:'ASC');
    IF WK002=1;
      WK319=VTTHOBJ_NUMDE(DENUM:570);                /* Next Propagation Date */
      TX018=OCSUB(SD208 1 1);                        /* Standard Usage Code 1 */
      IF (TX018='W') OR (TX018='P');                 /* If the fee is waived, follow Billed logic */
         TX002='B';
         IF TX018='W';                               /* If temporary waive, reset template folder */
            VTTDOBJ_VIEW(PLAN:SD001 FILEID:'ASC' TRAN:'245' PARTID:'000000000' FUND:'   ');
            LOOP WHILE VTTDOBJ_NEXT();
               IF VTTDOBJ_DE(581)='W';
                  VTTDOBJ_SETDE(DENUM:581 VALUE:' ');
                  WK002=VTTDOBJ_UPDATE();
                  IF WK002=0;
                     RPLOG_WARN(MSG:('Updating of ASC folder to clear StdUsCd1 for waiving fee errored'));
                  END;
               END;
            ENDLOOP;
         END;
      ELSE;
         TX002=PMPMOBJ_DE(DENUM:600);
         IF TX002=' ';
           RPLOG_WARN(MSG:('Asset Charge Deduct/Bill (PM600) option is not set.'));
           TX007='Y';
         END;
      END;
    ELSE;
      TX007='Y';
      RPLOG_ERROR(MSG:('ERROR reading ASC folder RECORD'));
    END;
    TX009='1';
    TX010=PH007;                          /* Save first participant through */
    TX020=PH007;                          /* For saving last participant through */
    WK200=0;WK203=0;WK205=0;WK206=0;WK207=0;
    OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:'STARTASC');  /* Though record does not exist yet, this sets key and sets up reading of DE's for plan's first pass */
    OCDATA_DESET(DENUM:500 VALUE:PL011);
    WK002=OCDATA_ITEMUPDATE();
    IF WK002=0;
      RPLOG_ERROR(MSG:('CONTAINER error when writing plan level assets and charge'));
    END;
  ELSE;
    RPLOG_WARN(MSG:('ASSET CHARGES ON HOLD FOR PLAN'));
  END;
ELSE;
  IF TX009='1';                           /* If first pass ... */
    IF PH007=TX010;                       /* Check if on first participant again */
      TX009='2';                          /* Indicate second pass                */
    ELSE;
      TX020=PH007;                        /* Save off last participant in first pass */
    END;
  END;
END;
IF TX007='Y';
   IF (TX009='2') AND (PH007=TX020);      /* If on last participant of second pass ... */
      OCDATA_CODESTROY(NAME:'ASC.T245.container');   /* End container usage for plan */
   END;
   QUIT;
END;
****************************** FIRST PASS **************************************
IF TX009='1';
  PERFORM 'CALC.PFBAL';
  WK001=WK200;
  IF WK200>0;
    IF TX002='D';                         /* Deduct */
      WK001=WK200;
    END;
    IF TX003='2';                         /* Plan level */
      WK205=WK205+WK200;                  /* Total participant balance */
    END;
  END;
***************************** SECOND PASS **************************************
ELSE;
  WK002=OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:PH007);
  IF WK002=1;
    WK200=OCDATA_NUMDE(DENUM:1);
    IF WK200>0;
      IF TX003='1';
        IF WK200<WK400;                   /* Compare to asset charge waive limit */
          PERFORM 'CALC.ASC';
          IF WK200<WK203;                 /* Don't charge more than EE has bal for  */
            WK203=WK200;
          END;
        ELSE;                             /* If above level of asset charge, zero out */
          WK203=0;
        END;
        IF TX002='D';                     /* Deduct the charge  */
          WK001=WK200;
          WK009=WK203;
        ELSE;                             /* Billed */
          WK207=WK207+WK203;
        END;
      ELSE;                               /* Plan Level */
        IF TX002='D';                     /* Deduct the charge  */
          WK001=WK200;
        END;
      END;
    END;
  ELSE;
    RPLOG_WARN(PARTID:PH008 MSG:('CONTAINER error getting participant assets'));
  END;
********************************************************************************
  IF PH007=TX020;                         /* If on last participant of first pass ... */
    IF TX002='B';                         /* Billed */
      IF WK207<>0;
        IF (TX018<>'W') AND (TX018<>'P');    /* If the fee is waived write a 901 memo instead of CAEXP */
          CAEXP_POSTBILL(SD206 "Asset Charge" WK207);  /* use SD206 instead of SD003 (CC 9675) */
        ELSE;
          SD033='   ';
          HI_BR_INIT;
          BR005@***=PL011;                   /* Plan ID Number */
          BR007@***='000000000';             /* Participant ID Number */
          BR008@***=SD206;                   /* Trade Date - changed to SD206 - CC 9675 */
          BR009@***=SD003;                   /* Run Date */
          BR010@***=SD004#999900/100;
          BR100@***='   ';                   /* Fund ID Number */
          BR101@***=245;                     /* Base Transaction Code */
          BR102@***=901;                     /* Activity Type */
          BR110@***=WK207;                   /* redifines Cash Field (BR110) */
          BR121@***=0;
          BR130@***=0;
          BR140@***=0;
          BR150@***=0;
          BR161@***=0;
          BR170@***=PL101;                   /* Post Number */
          BR271@***=WK025;                   /* Sub-Activity Code */
          HI_BR_WRITE;
        END;
      ELSE;
        RPLOG_WARN(MSG:('Plan Level Bill is zero; no bill created'));
      END;
    END;
    PMPMOBJ_SETDE(DENUM:319 VALUE:WK319); /* Next Asset Charge date */
    WK002=PMPMOBJ_UPDATE();
    IF WK002=0;
      RPLOG_WARN(MSG:('PM319 NOT UPDATED during Asset Charges'));
    END;
    OCDATA_CODESTROY(NAME:'ASC.T245.container');   /* End container usage for plan */
  END;
END;
********************************************************************************
********************           R O U T I N E ' S            ********************
********************************************************************************
ROUTINE 'CALC.PFBAL';
WK200=0;
KV001=0;
KV002=WK225;
EACH_PF@***;             /* TX004 = 'Y' means include fixed funds */
  IF ((TX004='Y') AND (FC700<>' ')) OR ((TX004='N') AND (FC700='V'));
    WK200=+PA999;        /* Adding all funds by participant cash balances   */
  END;
ENDEACH;
IF WK200<0;
  RPLOG_WARN(PARTID:PH008 FMT1:'CASH' AMT1:WK200 MSG:'Participant has negative balance');
  WK200=0;
END;
OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:PH007);
OCDATA_DESET(DENUM:1 VALUE:WK200);    /* Save off balance for second pass */
WK002=OCDATA_ITEMUPDATE();
IF WK002=0;
  RPLOG_ERROR(PARTID:PH008 MSG:('CONTAINER error when writing participant assets'));
END;
GOBACK;
****************************************
ROUTINE 'CALC.ASC';
WK201=TXTIER_GETVAL(TABLENAME:(TX006+"2") AMOUNT:WK200);
WK201=WK201/100;
IF TX005="B";                              /* For banded, need only 1 rate */
  WK203=WK200*WK201;                       /* Total asset charge based on 1st scale applied to part balance */
ELSE;                                      /* For FNN (first,next,next), need 2 rates & breakpoint */
  WK222=TXTIER_GETVAL(TABLENAME:(TX006+"1") AMOUNT:WK200);
  WK222=WK222/100;
  WK223=TXTIER_GETBREAK(TABLENAME:(TX006+"2") AMOUNT:WK200);
  WK203=(WK223*WK201)+((WK200-WK223)*WK222);    /* See Footnote #2 */
END;
WK203=OCNUM_ROUND(WK203 2);
IF WK203<WK405;                            /* Check that the asset charge is within min/max */
   WK203=WK405;
END;
IF WK203>WK410;
   WK203=WK410;
END;
IF TX008<>' ';                             /* Call for rates 465 is second scale    */
  WK202=TXTIER_GETVAL(TABLENAME:(TX008+"2") AMOUNT:WK200);
  WK202=WK202/100;
  IF TX005="B";                            /* For banded, need only 1 rate          */
    WK204=WK200*WK202;                     /* Total asset charge based on 2nd scale */
  ELSE;                                    /* For FNN, need 2 rates & breakpoint      */
    WK222=TXTIER_GETVAL(TABLENAME:(TX008+"1") AMOUNT:WK200);
    WK222=WK222/100;
    WK223=TXTIER_GETBREAK(TABLENAME:(TX008+"2") AMOUNT:WK200);
    WK204=(WK223*WK202)+((WK200-WK223)*WK222);   /* See Footnote #2 */
  END;
  WK204=OCNUM_ROUND(WK204 2);
  IF WK204<WK405;                          /* Check that the asset charge is within min/max */
    WK204=WK405;
  END;
  IF WK204>WK410;
    WK204=WK410;
  END;
  IF WK204<WK203;                          /* Better of --> ??? is this correct ??? */
    WK203=WK204;
  END;
END;
GOBACK;
************************************************************************************************
************************************************************************************************
*Footnote #1:
*Picks up rates from the CS scale table. Includes the 4 char table name type FNN or Banded
*rates.  If the rates are banded only need one rate, multiply by the assets and
*WK203 holds the asset charge.  If the table is FNN need to do a weighted average to get
*the charge
***************
*Footnote #2:
*Asset charge = table 2 rate * breakpoint +  table 1 rate * (actual CV - Breakpoint)
*For those of you trying to match this to the formula, skip division by CV and
*then multiplication by CV -> they cancel each other
***************
*Footnote #3:
*Same as Footnote #1 except using WK204. This is anticipation of having two
*scales to choose from, this is the second one.
***************
*Final notes:
*T245 calls this calculator twice for each participant.  It first goes through the participants
*one at a time then starts over with the first participant and loops through all of them again
*It does this twice since the T245 logic does not know if the charge is at Plan or Participant
*and the T245 'Allocation setting is set to Prorate (the default).
*
*                     Prorate to Participants           Participant Level (Flat)
* Pass 1     WK001    sum of PA999                      sum of PA999
*            WK008    0                                 0
*            WK009    0                                 0
* End of 1   WK001    Plan assets                       0
*            WK008    Plan Asset Charge                 0
*            WK009    0                                 0
* Pass 2     WK001    sum of PA999                      sum of PA999
*            WK008    0                                 0
*            WK009    0                                 Participants asset charge

e@  ������&B    O P6WAB C >Yp!D >Y}�E change to v3.0
M             G� F�********************************************************************************
**
**    Text File Name:  ASC-ASSETCHARGE
**    Text file for computing asset charges.  Should run automatically  T245
**    Designed for an all participant level run (when the participant entered on
**    T245 is all zeros) and for an the "Allocation" setting of "Prorated"
**    causing two passes to come through the calculator.
**
**    Inputs:
**    Variable List:
**     PL025 = product ID
**     FC700 = fund identifier V=Variable, F=Fixed,
**     PM400 = participant balance to waive charge
**     PM405 = minimum dollar asset fee
**     PM410 = maximum dollar asset fee
**     PM460 = asset charge scale #1
**     PM465 = asset charge scale #2
**     PM600 = scale #1 flag to deduct(D) or bill(B)
**     PM605 = scale #2 flag to deduct(D) or bill(B)
**     PM642 = flag to put asset charges on hold
**     PM643 = flag if two scales are they concurrent or the better of
**     PM645 = 1=Participant level  2=plan level
**     PM650 = use fixed funds in balance calculation y/n
**     PM655 = frequency of asset charges
**     PM660 = are scales Banded(B) or FNN(F)
**     PM665 = apply fee to fixed funds y/n
**
**     FC601 = set up for fund list, 1=variable, else a 0 T245 uses directly
**
**
**    Outputs:
**     PM319 = last asset charge date updated via this calculator
**     BR record
**
**    Transaction that starts this is T245, but runs automatically after the initial setup
**
**     COMPUTED VARIABLES
**     Series 200 for asset charges.  Note WK212 - WK217 predefined by Sungard, do not use
**     WK001 = predefined by Sungard to hold the balances
**     WK002 = Error flag
**     WK008 = predefined by Sungard to hold the plan level asset charge
**     WK009 = predefined by Sungard to hold the participant level asset charge
**     WK200 = cash balance
**     WK201 = rate for 1st asset charge   PM460
**     WK202 = second rate 2nd asset charge  PM465
**     WK203 = total $ to deduct based upon asset charge 1  PM460
**     WK204 = total $ to deduct based upon asset charge 2  PM465
**     WK205 = plan level assets
**     WK207 = Total Billed fee
**     WK209 = next date to set up asset charges
**     WK211 = flag =1 if the total plan assets have already been sent back via WK001
**     WK212 - WK220 PREDEFINED DO NOT USE
**     WK222 = if FNN need rate from B table
**     WK223 = if FNN need breakpoint for B table
**     WK225 = Trade or start date
**     WK319 = Next Propagation Date
**     WK400 = PM400
**     WK405 = PM405
**     WK410 = PM410
**     TX002 = bill/deduct flag bast on better of  PM460 use PM600     for PM465 use PM605
**     TX003 = PM645
**     TX004 = PM650
**     TX005 = PM660
**     TX006 = full name of asset charge scale using PM460 & PM660
**     TX007 = PM642
**     TX008 = full name of asset charge scale using PM465 & PM660
**     TX009 = FIRST PASS / SECOND indicator
**     TX010 = Hold first participant so know when first pass is complete
**     TX011 = Hold plan for verification that not on the next plan
**     TX018   Std Usage Code 1
**     TX020 = Hold last participant during first pass.
**
**  Note: This calculator is called for each participant, so no by participant loop needed.
**        Two passes are made. It goes through all participants once then does it again.
**
**    Created by:  Suzann Schiewer         05/19/2000
**
**    Modification History:
**    Programmer      DATE       CC    Reason for Change
**    Suzann Schiewer 02/12/01  3319   Change CABILL to CAEXP, Deduct option to D, and Bill reason
**    SPS             07/03/01  3531   change control - 1063 add XX like LN logic
**    Rick Sica       09/03/04  6187   Asset charge not being calculated correctly
**    Rick Sica       09/14/05  8111   One-time Waive of fee
**    Gary Pieratt    01/19/06  9675   Use trade date instead of run date
**    Judy Stewart    03/01/06  9709   Omniplus 5.5 Upgrade.
**    Rick Sica       03/24/06  10044  Reverse FNN (F1/F2) formula
**    Rick Sica       08/29/06  10617  901 records read/write differently for 5.5
********************************************************************************
SD080=10000;
WK001=0;WK008=0;WK009=0;
WK025=200;         /* Although this is on the Custom base tab, since it comes after the routines below, it is never hit */
IF PL011<>PH006;   /* On end of the first pass, the PH record as changed to the first part. of next plan */
  IF TX003='2';                                     /* Plan level asset charge */
    IF WK205>0;
      WK200=WK205;
      PERFORM 'CALC.ASC';
      RPLOG_INFO(FMT1:'DATE' FMT2:'CASH' AMT1:WK225 AMT2:WK200 MSG:'Plan Assets');
      IF TX002='D';                                 /* Deduct */
        WK001=WK200;
        WK008=WK203;                                /* Total fee to be prorated to all participants */
      ELSE;                                         /* Billed */
        WK207=WK203;
      END;
    ELSE;
      RPLOG_WARN(MSG:('Plan assets is zero; no fees applied.'));
    END;
  END;
  QUIT;
END;
WK002=OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:'STARTASC');
IF WK002=1;
  TX011=OCDATA_DE(DENUM:500);                       /* Get plan number of last plan through */
ELSE;
  TX011=' ';
END;
********************************************************************************
IF PL011<>TX011;                                    /* If new plan ... */
  OCDATA_CODESTROY(NAME:'ASC.T245.container');      /* start a fresh container */
  OCDATA_COCREATE(NAME:'ASC.T245.container' KEYLENG:9 TX10S:1 NUMS:2);
  WK002=PMPMOBJ_GET(PLAN:SD001 PRODUCTID:PL025);
  IF WK002=0;
    TX007='Y';                                      /* Stop further passes for plan */
    RPLOG_ERROR(MSG:'ERROR READING PM RECORD');
    QUIT;
  ELSE;
    TX007=PMPMOBJ_DE(DENUM:642);          /* The following data elements are not set but here and are held throughout the plan */
  END;
  IF TX007="N";                           /* Asset charges NOT on hold */
    WK400=PMPMOBJ_NUMDE(DENUM:400);
    IF WK400<=0;
      RPLOG_WARN(MSG:('Asset charge waive fee (PM400) set to 0; no fees will be calculated.'));
      TX007='Y';
    END;
    WK405=PMPMOBJ_NUMDE(DENUM:405);
    WK410=PMPMOBJ_NUMDE(DENUM:410);
    IF WK410<=0;
      RPLOG_WARN(MSG:('Asset charge maximum (PM410) set to 0; assuming unlimited.'));
      WK410=9999999.99;
    END;
    TX003=PMPMOBJ_DE(DENUM:645);
    IF TX003=' ';
      RPLOG_WARN(MSG:('Asset Chrg Acct Bal Calc Level (PM645) option is not set; Plan level assumed.'));
      TX003='2';
    END;
    TX004=PMPMOBJ_DE(DENUM:650);
    IF TX004=' ';
      RPLOG_WARN(MSG:('Asset Chrg Fixed Fund in Bal Calc (PM650) option is not set; Fixed funds included.'));
      TX004='Y';
    END;
    TX005=PMPMOBJ_DE(DENUM:660);
    IF TX005=' ';
      RPLOG_WARN(MSG:('Asset Charge FNN/Banded (PM660) option is not set.'));
      TX007='Y';
    END;
    TX006=PMPMOBJ_DE(DENUM:460);
    IF TX006<>' ';
      TX006="CS"+OCSUB(TX006 1 4)+TX005;  /* For first scale in PM460; See Footnote #1 */
    ELSE;
      RPLOG_WARN(MSG:('Asset Charge 1 scale not set (PM460)'));
      TX007='Y';
    END;
    TX008=PMPMOBJ_DE(DENUM:465);
    IF TX008<>' ';
      TX008="CS"+OCSUB(TX008 1 4)+TX005;             /* See Footnote #3 */
    END;
    WK225=SD206;                                     /* current trade date */
    WK002=VTTHOBJ_GET(PLAN:SD001 FILEID:'ASC');
    IF WK002=1;
      WK319=VTTHOBJ_NUMDE(DENUM:570);                /* Next Propagation Date */
      TX018=OCSUB(SD208 1 1);                        /* Standard Usage Code 1 */
      IF (TX018='W') OR (TX018='P');                 /* If the fee is waived, follow Billed logic */
         TX002='B';
         IF TX018='W';                               /* If temporary waive, reset template folder */
            VTTDOBJ_VIEW(PLAN:SD001 FILEID:'ASC' TRAN:'245' PARTID:'000000000' FUND:'   ');
            LOOP WHILE VTTDOBJ_NEXT();
               IF VTTDOBJ_DE(581)='W';
                  VTTDOBJ_SETDE(DENUM:581 VALUE:' ');
                  WK002=VTTDOBJ_UPDATE();
                  IF WK002=0;
                     RPLOG_WARN(MSG:('Updating of ASC folder to clear StdUsCd1 for waiving fee errored'));
                  END;
               END;
            ENDLOOP;
         END;
      ELSE;
         TX002=PMPMOBJ_DE(DENUM:600);
         IF TX002=' ';
           RPLOG_WARN(MSG:('Asset Charge Deduct/Bill (PM600) option is not set.'));
           TX007='Y';
         END;
      END;
    ELSE;
      TX007='Y';
      RPLOG_ERROR(MSG:('ERROR reading ASC folder RECORD'));
    END;
    TX009='1';
    TX010=PH007;                          /* Save first participant through */
    TX020=PH007;                          /* For saving last participant through */
    WK200=0;WK203=0;WK205=0;WK206=0;WK207=0;
    OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:'STARTASC');  /* Though record does not exist yet, this sets key and sets up reading of DE's for plan's first pass */
    OCDATA_DESET(DENUM:500 VALUE:PL011);
    WK002=OCDATA_ITEMUPDATE();
    IF WK002=0;
      RPLOG_ERROR(MSG:('CONTAINER error when writing plan level assets and charge'));
    END;
  ELSE;
    RPLOG_WARN(MSG:('ASSET CHARGES ON HOLD FOR PLAN'));
  END;
ELSE;
  IF TX009='1';                           /* If first pass ... */
    IF PH007=TX010;                       /* Check if on first participant again */
      TX009='2';                          /* Indicate second pass                */
    ELSE;
      TX020=PH007;                        /* Save off last participant in first pass */
    END;
  END;
END;
IF TX007='Y';
   IF (TX009='2') AND (PH007=TX020);      /* If on last participant of second pass ... */
      OCDATA_CODESTROY(NAME:'ASC.T245.container');   /* End container usage for plan */
   END;
   QUIT;
END;
****************************** FIRST PASS **************************************
IF TX009='1';
  PERFORM 'CALC.PFBAL';
  WK001=WK200;
  IF WK200>0;
    IF TX002='D';                         /* Deduct */
      WK001=WK200;
    END;
    IF TX003='2';                         /* Plan level */
      WK205=WK205+WK200;                  /* Total participant balance */
    END;
  END;
***************************** SECOND PASS **************************************
ELSE;
  WK002=OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:PH007);
  IF WK002=1;
    WK200=OCDATA_NUMDE(DENUM:1);
    IF WK200>0;
      IF TX003='1';
        IF WK200<WK400;                   /* Compare to asset charge waive limit */
          PERFORM 'CALC.ASC';
          IF WK200<WK203;                 /* Don't charge more than EE has bal for  */
            WK203=WK200;
          END;
        ELSE;                             /* If above level of asset charge, zero out */
          WK203=0;
        END;
        IF TX002='D';                     /* Deduct the charge  */
          WK001=WK200;
          WK009=WK203;
        ELSE;                             /* Billed */
          WK207=WK207+WK203;
        END;
      ELSE;                               /* Plan Level */
        IF TX002='D';                     /* Deduct the charge  */
          WK001=WK200;
        END;
      END;
    END;
  ELSE;
    RPLOG_WARN(PARTID:PH008 MSG:('CONTAINER error getting participant assets'));
  END;
********************************************************************************
  IF PH007=TX020;                         /* If on last participant of first pass ... */
    IF TX002='B';                         /* Billed */
      IF WK207<>0;
        IF (TX018<>'W') AND (TX018<>'P');    /* If the fee is waived write a 901 memo instead of CAEXP */
          CAEXP_POSTBILL(SD206 "Asset Charge" WK207);  /* use SD206 instead of SD003 (CC 9675) */
        ELSE;
          SD033='   ';
          HI_BR_INIT;
          BR005@***=PL011;                   /* Plan ID Number */
          BR007@***='000000000';             /* Participant ID Number */
          BR008@***=SD206;                   /* Trade Date - changed to SD206 - CC 9675 */
          BR009@***=SD003;                   /* Run Date */
          BR010@***=SD004#999900/100;
          BR100@***='   ';                   /* Fund ID Number */
          BR101@***=245;                     /* Base Transaction Code */
          BR102@***=901;                     /* Activity Type */
          BR110@***=WK207;                   /* redifines Cash Field (BR110) */
          BR121@***=0;
          BR130@***=0;
          BR140@***=0;
          BR150@***=0;
          BR161@***=0;
          BR170@***=PL101;                   /* Post Number */
          BR271@***=WK025;                   /* Sub-Activity Code */
          HI_BR_WRITE;
        END;
      ELSE;
        RPLOG_WARN(MSG:('Plan Level Bill is zero; no bill created'));
      END;
    END;
    PMPMOBJ_SETDE(DENUM:319 VALUE:WK319); /* Next Asset Charge date */
    WK002=PMPMOBJ_UPDATE();
    IF WK002=0;
      RPLOG_WARN(MSG:('PM319 NOT UPDATED during Asset Charges'));
    END;
    OCDATA_CODESTROY(NAME:'ASC.T245.container');   /* End container usage for plan */
  END;
END;
********************************************************************************
********************           R O U T I N E ' S            ********************
********************************************************************************
ROUTINE 'CALC.PFBAL';
WK200=0;
KV001=0;
KV002=WK225;
EACH_PF@***;             /* TX004 = 'Y' means include fixed funds */
  IF ((TX004='Y') AND (FC700<>' ')) OR ((TX004='N') AND (FC700='V'));
    WK200=+PA999;        /* Adding all funds by participant cash balances   */
  END;
ENDEACH;
IF WK200<0;
  RPLOG_WARN(PARTID:PH008 FMT1:'CASH' AMT1:WK200 MSG:'Participant has negative balance');
  WK200=0;
END;
OCDATA_ITEMGET(NAME:'ASC.T245.container' KEY:PH007);
OCDATA_DESET(DENUM:1 VALUE:WK200);    /* Save off balance for second pass */
WK002=OCDATA_ITEMUPDATE();
IF WK002=0;
  RPLOG_ERROR(PARTID:PH008 MSG:('CONTAINER error when writing participant assets'));
END;
GOBACK;
****************************************
ROUTINE 'CALC.ASC';
WK201=TXTIER_GETVAL(TABLENAME:(TX006+"2") AMOUNT:WK200);
WK201=WK201/100;
IF TX005="B";                              /* For banded, need only 1 rate */
  WK203=WK200*WK201;                       /* Total asset charge based on 1st scale applied to part balance */
ELSE;                                      /* For FNN (first,next,next), need 2 rates & breakpoint */
  WK222=TXTIER_GETVAL(TABLENAME:(TX006+"1") AMOUNT:WK200);
  WK222=WK222/100;
  WK223=TXTIER_GETBREAK(TABLENAME:(TX006+"2") AMOUNT:WK200);
  WK203=(WK223*WK201)+((WK200-WK223)*WK222);    /* See Footnote #2 */
END;
WK203=OCNUM_ROUND(WK203 2);
IF WK203<WK405;                            /* Check that the asset charge is within min/max */
   WK203=WK405;
END;
IF WK203>WK410;
   WK203=WK410;
END;
IF TX008<>' ';                             /* Call for rates 465 is second scale    */
  WK202=TXTIER_GETVAL(TABLENAME:(TX008+"2") AMOUNT:WK200);
  WK202=WK202/100;
  IF TX005="B";                            /* For banded, need only 1 rate          */
    WK204=WK200*WK202;                     /* Total asset charge based on 2nd scale */
  ELSE;                                    /* For FNN, need 2 rates & breakpoint      */
    WK222=TXTIER_GETVAL(TABLENAME:(TX008+"1") AMOUNT:WK200);
    WK222=WK222/100;
    WK223=TXTIER_GETBREAK(TABLENAME:(TX008+"2") AMOUNT:WK200);
    WK204=(WK223*WK202)+((WK200-WK223)*WK222);   /* See Footnote #2 */
  END;
  WK204=OCNUM_ROUND(WK204 2);
  IF WK204<WK405;                          /* Check that the asset charge is within min/max */
    WK204=WK405;
  END;
  IF WK204>WK410;
    WK204=WK410;
  END;
  IF WK204<WK203;                          /* Better of --> ??? is this correct ??? */
    WK203=WK204;
  END;
END;
GOBACK;
************************************************************************************************
************************************************************************************************
*Footnote #1:
*Picks up rates from the CS scale table. Includes the 4 char table name type FNN or Banded
*rates.  If the rates are banded only need one rate, multiply by the assets and
*WK203 holds the asset charge.  If the table is FNN need to do a weighted average to get
*the charge
***************
*Footnote #2:
*Asset charge = table 2 rate * breakpoint +  table 1 rate * (actual CV - Breakpoint)
*For those of you trying to match this to the formula, skip division by CV and
*then multiplication by CV -> they cancel each other
***************
*Footnote #3:
*Same as Footnote #1 except using WK204. This is anticipation of having two
*scales to choose from, this is the second one.
***************
*Final notes:
*T245 calls this calculator twice for each participant.  It first goes through the participants
*one at a time then starts over with the first participant and loops through all of them again
*It does this twice since the T245 logic does not know if the charge is at Plan or Participant
*and the T245 'Allocation setting is set to Prorate (the default).
*
*                     Prorate to Participants           Participant Level (Flat)
* Pass 1     WK001    sum of PA999                      sum of PA999
*            WK008    0                                 0
*            WK009    0                                 0
* End of 1   WK001    Plan assets                       0
*            WK008    Plan Asset Charge                 0
*            WK009    0                                 0
* Pass 2     WK001    sum of PA999                      sum of PA999
*            WK008    0                                 0
*            WK009    0                                 Participants asset charge

@  ������"J  U P6WAB P  �Z   T  W 
 H ASC-ASSETCHARGE I Initial Load
X >YxS 	Baseline    S change to v3.0    ^ Production    @  ���e���n