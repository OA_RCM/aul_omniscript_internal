� Polytron VCS logfile A  @      ����B    O P6WAB C >Yp�D >YxsE Initial revision.
F� HEi� H=  ***********************************************************************************************
***********************************************************************************************
**
**  Text File Name : TRN-FFMINMAX
**  Sets the WF's needed.  If the calling script is IFT, then this one will
**  look at the participants balance.  If it is below the maximum to apply limts continue
**  If any of the by fund (Fixed) is not less than minimum to apply percent, then apply percent
**  Also should apply percent from any calling script that is not IFT (inter fund transfer)
**
**  Inputs
**  IC322   Maximum Transfer Percentage
**  TX002   Passed from calling script, holds the From Investment fund id, 2 chars
**  TX005   From Fund identifier  IIS, pased from the calling script
**  TX009   To hold the From Fund Source identifier, passed from the calling script
**  TX012   3 char key from calling script,
**          calling xfermax            tx012 value
**            02 Inter Fund Transfer      IFT
**            04 Rebalancing              BAL
**  KV001   Passed from calling script, holds 0 starting date of balance
**  KV002   Passed from calling script, holds trade date, ending date of balance
**  FC700   F=fixed fund indicator
**  PM411   Maximum fund balance to apply transfer limits - based on sum total of participants $
**  PM412   Minimum fund balance to apply transfer limits - based on by investment only
**  PM662   Transfer balance calculation method M=market value as of trade date B=Begin of Contract Yr
**  WK018   Passed from calling script, holds the PYE date from PLYEAR
**  WK019   Holds the $ value of request, from the Properites part of T381 or % converted
**  PF609   Conversion contract year beginning balance for fixed fund 1
**  PF610   Conversion contract year beginning balance for fixed fund 2
**  PF611   Conversion transfers in the current plan year for fixed fund 1
**  PF612   Conversion transfers in the current plan year for fixed fund 2
**
**  Outputs      :
**  WF004   Maximum amount for transfer
**  WF005   Maximum number of shares
**  WF006   Maximum uninvested cash available
**
**  Called by XFERMAX, T381 calculators (02, etc)
**
**  Variable List:
**
**  TM001   Omni predefined holds error message
**  TX003   As each fund is looped through, this holds the Investment fund ID
**  TX004   Under the ASSUMPTION that there are only 2 fixed funds, this holds the first ID
**  TX010   fund id of fund in a loop
**  TX011   source id of fund in a loop
**  TX013   N - do not deduct prior transfers   Y - do deduct prior transfers
**  TX014   Same as tx013, but for 2nd fixed fund
**  WK001 - WK011 Predefined for this function, do not use internally
**  WK015   Sum of the first fixed fund, if only one then this is it - investment total
**  WK016   Sum of the Second fixed fund (assume only 2 at most) - investment total
**  WK017   Sum of the participants funds - total
**  WK028   Holder for trade date
**  WK029   Holds the sum of funds involved with transfer
**  WK030   Predefined as the error flag 1=error 0=no error
**  WK035   Sum of the first fixed fund, - investment total only if FC063 >1, transfered allowed
**  WK036   Sum of the Second fixed fund - investment total only if FC063 >1, transfered allowed
**  WK045   Declining allowable Balance for first fixed fund, starts = wk015*ic302
**  WK046   Declining allowable Balance for second fixed fund, starts = wk016*ic302
**  WK050   current pye beginning date
**  WK051   PM345 which is the conversion date
**  WK053   current pye date
**  WK054   indicator = 1 if this is a conversion plan and this transaction is running in
**          the conversion year
**
**  Created By                         Date           SMR
**  ==============================     ========     =======
**  Suzann Schiewer                    09/25/2000    3176
**
**
**  Modification History:
**
**  Programmer                         Date           SMR
**  ==============================     ========     ========
**  sps                                09/27/2002    cc 2049 remove if bye=0,use current values
**  sps                                10/10/2002    cc3697  ff transfer limit should be spread
**                                                          across allowable funds
**  SPS                                10/17/2002    CC3784  EXCLUDE USAGE CODE T
**  SPS                                11/15/2002    CC3873  CHANGE FORMULA (A/B) * B
**                                                           TO (A) * (B/B), ROUNDING ERROR
**  Sue Freas                          07/10/03      CC4889  Added logic to skip ER directed
**                                                             sources
**  Susan Long                         10/20/2003    CC5094  Change all PF values to PA
**  Sue Freas                          10/28/03      CC4093
**    Reason: Rebalancing not using current market value, affecting 20% restriction
**  Susan Long                         12/01/03      cc5585
**    Reason: Harris Trust needs to go thru this code for $ transfers.  Skipping code for most
**            edits but still going thru the prorata code.
**  Jodi Stone/Sue Freas               03/31/2004    cc5974
**    Reason: For conversion, calculator needs to look at UDFs that store MAAS transfer amounts
**            and MAAS Beginning Balances for participants.
**  Jodi Stone                         07/01/2004    CC6548
**    Reason:  Added code so the 1st fixed fund can be either I2 or I4 and the calculator can now
**             handle a transfer that has both fixed funds.  Also, commented out is the reject message
**             when a transfer is from one fixed fund to the other fixed fund.
**  Susan Long                         11/12/2004    cc7212
**    Reason:  If H1 for conversion plan, check if the trade date year is in the conversion
**             year PM345 year.  If so, there would be no beginning balance but need to send
**             thru the 20% restriction code. Made changes to logic in cc5974 and cc6548.
**             WK054 set to 1 if this is a conversion plan in the conversion year.
**  Susan Long                         09/14/2005    cc9028
**    Reason:  Add INITKV to reinitialize all kv values.
**  Susan Long                          07/28/2005   cc6615
**    Reason:  Remove hardcoded fixed fund investments
**  Susan Long                          09/14/2005   cc8862
**    Reason:  20% fixed interest transfer restriction corrections.  Basically redid
**             much of the logic in this code.
**  Susan Long                          11/11/2005   cc9293
**    Reason:  For conversion plans, the beginning ff balances and transfers were entered into
**             PF609/610(beginning bal) and PF611/612(transfers) for FF1 and FF2 respectively.
**             Now that GIF and LIF are being converted also, these values all need to be set
**             in only PF609 and PF611 since it will not be known which will correspond to
**             which fund.  ONE-CC9293 will need to be run before this is moved and will also
**             need to be run after each conversion week-end run.
**  Susan Long                          12/14/05     possible production problem
**             Set max loops
**  Judy Stewart                        03/01/2006   cc9709
**    Reason:  Omniplus 5.5 Upgrade.
**  Susan Long                         05/24/2006    CC8898
**   Reason: Make functionality chgs to update/improve code per Sungard suggestions.
**           Remove code about 'true' percentage because if amt type=1, it is now 'true' percentage
**           removing the dollar amount from the properties tab to the 'true' dollar field.
**           Put IFs around the IVICOBJ_GETs
**           Remove BATCUT_FETCHCURR() not needed since it is done in the calling calculator
**  Susan Huppert                      07/16/2007    CC11784
**   Reason: Correct fixed fund restrictions to allow different transfer amounts
**  Judy Stewart                       10/08/2009    WMS1519B
**   Reason: Omniplus 5.8 Upgrade
**  Danny Hagans                       06/20/2010    WMS4463
**   Reason: SDB % transfers
**************************************************************************************
WK028 = KV002;                     /* Hold on to trade date for resetting    */
TX004 = "";
WK029 = 0;
TX067="N";
SD080=999999999;
*Sum of participants funds and funds of the transfer are current market value
EACH@***;
   WF004=0;
   WF005=0;
   WF006=0;
   WF008=.01; /* WMS1519B */
   IF (IC008<>"LN") AND (IC008<>"LD") AND (IC008<>"XX"); /* WMS1519B */
      WK017=+PA999;    /* Sum total of participants funds, not loans   */
      IF ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
         ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
         (SC602="1");
         WF004=PA999;
         WF005=PA130;
         WF006=PA100;
         WF008=0;      /* WMS1519B */
         WK029=+PA999; /*holds sum of funds involved with the transfer for prorata */
         IF FC700="F";
            TX067="Y";
         END;
      END;
   END;
ENDEACH;
TX013 = 'Y';
TX014 = 'Y';
*Don't deduct prior transfers if this is a Sweeps
* or if the total account balance of the participant > maximum fund balance
IF TX012 = 'SWP';
   TX013 = 'N';
   TX014 = 'N';
END;
IF ((WK017 > PMPMOBJ_NUMDE(411)) AND (TX012 = 'IFT'));
   TX013 = 'N';
   TX014 = 'N';
END;
*determine if a conversion plan
WK050 = OCDATE_BUMP(DATE:WK018 DAY:1);  /* current pye beginning */
WK053 = OCDATE_BUMP(DATE:WK018 YEAR:1); /* current pye */
WK051 = PMPMOBJ_NUMDE(345);             /* Conversion date*/
IF (WK051 > WK050) AND (WK051 < WK053); /* if conv is between beg & end pye */
   WK054=1;  /* this plan is a conversion plan in the conversion year */
ELSE;
   WK054=0;
END;
************************************************************************************************
*if harris trust and there is a fixed from fund, need to do the 20% transfer check
* CC8898 to check specifically for H1 instead of the old code that said <> H2 which would
* get all the plans coded with PM723=any other code like N1
IF (TX067="Y") AND (PMPMOBJ_DE(723) = "H1"); /* if there is a fixed from fund */
   IF (PMPMOBJ_DE(662) = "B");
      KV002=WK018;                /* based on beginning of contract year */
   ELSE;
      KV002=WK028;                /* based on the trade date */
   END;
   EACH@***;
      TX003 = OCTEXT_SUB(FC026 1 2);   /*  investment id */
      IF FC700="F";
         IF (TX004="") OR (TX004=TX003); /* FF1 */
            TX004=TX003;          /* Holds first FF */
            IF WK054=1;           /* plan in conversion year, get from PF609 */
               WK015=+PF609;
            ELSE;
               IF IC322=100;      /* cc11784 if IC322=100, transfer from trade date bal */
                  KV002=WK028;
               END;
               WK015=+PA999;         /* first FF balance */
            END;
         ELSE;                    /* must be the second fixed fund */
            TX044=TX003;          /* TX044 will hold the FF2 */
            IF WK054=1;           /* plan in conversion year, get from PF609 */
               WK016=+PF609;
            ELSE;
               IF IC322=100;      /* cc11784 if IC322=100, transfer from trade date bal */
                  KV002=WK028;
               END;
               WK016=+PA999;         /* second FF balance */
            END;
         END;
      END;
   ENDEACH;
***This will cause the current market value to be used, and no deducts
   IF (((TX012='IFT') OR (TX012='REB')) AND (WK015 < PMPMOBJ_NUMDE(412)));
      TX013 = 'N';           /* CC4093: Added REB to above */
   END;
   IF (((TX012='IFT') OR (TX012='REB')) AND (WK016 < PMPMOBJ_NUMDE(412)));
      TX014 = 'N';           /* CC4093: Added REB to above */
   END;
***not sure we should do this?????
   IF ((TX012='IFT') OR (TX012='REB')) AND     /* if IFT or REB transfer and  */
      ((PMPMOBJ_DE(723)="H1") AND (WK054=1));  /*  if harris trust and a conversion pln in yr*/
      TX013='Y';
      TX014='Y';
   END;
*  get max available to transfer
   IF TX004<>"";
      IF IVICOBJ_GET(PLAN:SD001 FUNDIV:TX004);  /* get investment control for FF1 */
         WK045=WK015*(OCTEXT_TONUM(IVICOBJ_DE(322))/100); /* usually 20% value in IC322 */
         IF IVICOBJ_NUMDE(322)=100;    /* cc11784 if IC322=100,do not deduct prior xfers  */
            TX013="N";
         END;
      END;
   END;
   IF TX044<>"";
      IF IVICOBJ_GET(PLAN:SD001 FUNDIV:TX044);  /* get investment control for FF2 */
         WK046=WK016*(OCTEXT_TONUM(IVICOBJ_DE(322))/100);
         IF IVICOBJ_NUMDE(322)=100;    /* cc11784 if IC322=100,do not deduct prior xfers  */
            TX014="N";
         END;
      END;
   END;
*  Need to deduct prior transfer dollars based upon contract year
   IF (TX013 = 'Y') OR (TX014 = 'Y');
      IF PMPMOBJ_DE(662) = "B";
         WK027=OCDATE_ADDDAYS(WK018 1);  /* get one day after start of PYE */
      ELSE;
         WK027=OCDATE_BUMP(WK028 YEAR:-1 DAY:1); /* go back 1 year plus 1 day from TD */
      END;
      KV002=WK028;                     /* trade date */
      HIBROBJ_VIEW(PLAN:SD001 PARTID:SD002 TRADEDATELO:WK027 TRADEDATEHI:WK028
                   TRAN:'381' ACTIVITY:021 STATUS:'ACTIVE');
      LOOP WHILE HIBROBJ_NEXT();
         IF FNFCOBJ_GET(PLAN:SD001 FUND:HIBROBJ_DE(100));
            IF FNFCOBJ_DE(700)="F";
               IF (TX013="Y") AND (OCTEXT_SUB(HIBROBJ_DE(100) 1 2)=TX004) AND /* FF1 */
                  (OCFIND(HIBROBJ_DE(105) 'I' 'C' 'T')=0);
                  WK045=WK045-HIBROBJ_NUMDE(110);
               END;
               IF (TX014="Y") AND (OCTEXT_SUB(HIBROBJ_DE(100) 1 2)=TX044) AND /* FF2 */
                  (OCFIND(HIBROBJ_DE(105) 'I' 'C' 'T')=0);
                  WK046=WK046-HIBROBJ_NUMDE(110);
               END;
            END;
         END;
      ENDLOOP;
      IF WK054=1;    /* if conv plan in conv year, subtract the maas xfer amts also */
         EACH@***;
            IF IC008=TX004;        /* FF1 */
               WK045=WK045-PF611;  /* also subtract out the transfers from Maas */
            ELSEIF IC008=TX044;    /* FF2 */
               WK046=WK046-PF611;  /* also subtract out the transfers from Maas */
            END;
         ENDEACH;
      END;
   END;
   INITKV;
   KV002=WK028;          /* Reset KV002 to trade date  */
   EACH@***;
      IF (FC700="F") AND
         ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
         ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
         (SC602="1");
         IF (IC008=TX004) AND
            (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
            IF OCTEXT_TONUM(FC063) < 2;
               WK035=WK035+PA999;
            END;
         ELSEIF (IC008=TX044) AND
            (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
            IF OCTEXT_TONUM(FC063) < 2;
               WK036=WK036+PA999;
            END;
         END;
      END;
   ENDEACH;
*  prorate the fixed from funds
   EACH@***;
      IF (FC700="F") AND
         ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
         ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
         (SC602="1");
         IF ((WK017 <= PMPMOBJ_NUMDE(411)) AND (TX012 = 'IFT')) OR (TX012 >< 'IFT');
            IF (IC008=TX004) AND  /* FF1 */
               (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
               IF ((TX012='IFT') AND (WK015 >= PMPMOBJ_NUMDE(412))) OR (TX012 >< 'IFT');
                  IF FC026=BATCUT_TRANFUND();  /* if from is IIS */
                     WF004=WK045;  /* the FF1 20% minus xfers */
                  ELSE;
                     WF004 = (PA999/WK035) * (WK045);
                     WK035 = WK035 - PA999;
                     WK045 = WK045 - WF004;
                  END;
               END;
            ELSEIF (IC008=TX044) AND /* FF2 */
               (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
               IF ((TX012='IFT') AND (WK016>=PMPMOBJ_NUMDE(412))) OR (TX012 >< 'IFT');
                  IF FC026=BATCUT_TRANFUND(); /* if from is IIS */
                     WF004=WK046;  /* the FF1 20% minus xfers */
                  ELSE;
                     WF004 = (PA999/WK036) * (WK046);
                     WK036 = WK036 - PA999;
                     WK046 = WK046 - WF004;
                  END;
               END;
            END;
         END;
      END;
   ENDEACH;
END;                       /* CC5585 end of if not a harris trust plan */
***********************************************************************************************
*the following is now done for all the from funds
IF TX012='REB';
   EACH@***;
      IF WF004 > PA999;
         WF004 = PA999;
      END;
   ENDEACH;
END;
EACH@***;
   IF ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
      ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
      (SC602="1");
      WK068=(PA999)*(WK019/WK029);  /*acct bal times (xfer amt divided by sum of from funds*/
      IF WF004>WK068;
         WF004=WK068;
      END;
      IF WF004<.01;
         WF004=0;
         WF008=.01; /* WMS1519B */
      END;
      IF (BATCUT_NUMVTDE(430)=0) AND (BATCUT_DE(074)="1"); /* true percentage */
        WK069=BATCUT_NUMDE(076)*PA999; /* WMS1519B - percentage times acct bal of from fund */
        IF WF004<WK069; /* WMS1519B */
           WF004=WF004/BATCUT_NUMDE(076); /* WMS1519B - set limit so when Omni takes the %, it will */
        ELSE;               /* WMS1519B  figure the correct limit */
*           IF TX033<>'Y';    /* WMS1519B  only if not an SD transfer */
             WF004=PA999;    /* if limit exceeds the percent, set so Omni will get the true % */
*           END;              /* WMS1519B */
        END;
     END;
   END;
ENDEACH;
e@  ���t���}B    O P6WAB C >Yp�D >Y}�E change to v3.0
M             G� H=***********************************************************************************************
***********************************************************************************************
**
**  Text File Name : TRN-FFMINMAX
**  Sets the WF's needed.  If the calling script is IFT, then this one will
**  look at the participants balance.  If it is below the maximum to apply limts continue
**  If any of the by fund (Fixed) is not less than minimum to apply percent, then apply percent
**  Also should apply percent from any calling script that is not IFT (inter fund transfer)
**
**  Inputs
**  IC322   Maximum Transfer Percentage
**  TX002   Passed from calling script, holds the From Investment fund id, 2 chars
**  TX005   From Fund identifier  IIS, pased from the calling script
**  TX009   To hold the From Fund Source identifier, passed from the calling script
**  TX012   3 char key from calling script,
**          calling xfermax            tx012 value
**            02 Inter Fund Transfer      IFT
**            04 Rebalancing              BAL
**  KV001   Passed from calling script, holds 0 starting date of balance
**  KV002   Passed from calling script, holds trade date, ending date of balance
**  FC700   F=fixed fund indicator
**  PM411   Maximum fund balance to apply transfer limits - based on sum total of participants $
**  PM412   Minimum fund balance to apply transfer limits - based on by investment only
**  PM662   Transfer balance calculation method M=market value as of trade date B=Begin of Contract Yr
**  WK018   Passed from calling script, holds the PYE date from PLYEAR
**  WK019   Holds the $ value of request, from the Properites part of T381 or % converted
**  PF609   Conversion contract year beginning balance for fixed fund 1
**  PF610   Conversion contract year beginning balance for fixed fund 2
**  PF611   Conversion transfers in the current plan year for fixed fund 1
**  PF612   Conversion transfers in the current plan year for fixed fund 2
**
**  Outputs      :
**  WF004   Maximum amount for transfer
**  WF005   Maximum number of shares
**  WF006   Maximum uninvested cash available
**
**  Called by XFERMAX, T381 calculators (02, etc)
**
**  Variable List:
**
**  TM001   Omni predefined holds error message
**  TX003   As each fund is looped through, this holds the Investment fund ID
**  TX004   Under the ASSUMPTION that there are only 2 fixed funds, this holds the first ID
**  TX010   fund id of fund in a loop
**  TX011   source id of fund in a loop
**  TX013   N - do not deduct prior transfers   Y - do deduct prior transfers
**  TX014   Same as tx013, but for 2nd fixed fund
**  WK001 - WK011 Predefined for this function, do not use internally
**  WK015   Sum of the first fixed fund, if only one then this is it - investment total
**  WK016   Sum of the Second fixed fund (assume only 2 at most) - investment total
**  WK017   Sum of the participants funds - total
**  WK028   Holder for trade date
**  WK029   Holds the sum of funds involved with transfer
**  WK030   Predefined as the error flag 1=error 0=no error
**  WK035   Sum of the first fixed fund, - investment total only if FC063 >1, transfered allowed
**  WK036   Sum of the Second fixed fund - investment total only if FC063 >1, transfered allowed
**  WK045   Declining allowable Balance for first fixed fund, starts = wk015*ic302
**  WK046   Declining allowable Balance for second fixed fund, starts = wk016*ic302
**  WK050   current pye beginning date
**  WK051   PM345 which is the conversion date
**  WK053   current pye date
**  WK054   indicator = 1 if this is a conversion plan and this transaction is running in
**          the conversion year
**
**  Created By                         Date           SMR
**  ==============================     ========     =======
**  Suzann Schiewer                    09/25/2000    3176
**
**
**  Modification History:
**
**  Programmer                         Date           SMR
**  ==============================     ========     ========
**  sps                                09/27/2002    cc 2049 remove if bye=0,use current values
**  sps                                10/10/2002    cc3697  ff transfer limit should be spread
**                                                          across allowable funds
**  SPS                                10/17/2002    CC3784  EXCLUDE USAGE CODE T
**  SPS                                11/15/2002    CC3873  CHANGE FORMULA (A/B) * B
**                                                           TO (A) * (B/B), ROUNDING ERROR
**  Sue Freas                          07/10/03      CC4889  Added logic to skip ER directed
**                                                             sources
**  Susan Long                         10/20/2003    CC5094  Change all PF values to PA
**  Sue Freas                          10/28/03      CC4093
**    Reason: Rebalancing not using current market value, affecting 20% restriction
**  Susan Long                         12/01/03      cc5585
**    Reason: Harris Trust needs to go thru this code for $ transfers.  Skipping code for most
**            edits but still going thru the prorata code.
**  Jodi Stone/Sue Freas               03/31/2004    cc5974
**    Reason: For conversion, calculator needs to look at UDFs that store MAAS transfer amounts
**            and MAAS Beginning Balances for participants.
**  Jodi Stone                         07/01/2004    CC6548
**    Reason:  Added code so the 1st fixed fund can be either I2 or I4 and the calculator can now
**             handle a transfer that has both fixed funds.  Also, commented out is the reject message
**             when a transfer is from one fixed fund to the other fixed fund.
**  Susan Long                         11/12/2004    cc7212
**    Reason:  If H1 for conversion plan, check if the trade date year is in the conversion
**             year PM345 year.  If so, there would be no beginning balance but need to send
**             thru the 20% restriction code. Made changes to logic in cc5974 and cc6548.
**             WK054 set to 1 if this is a conversion plan in the conversion year.
**  Susan Long                         09/14/2005    cc9028
**    Reason:  Add INITKV to reinitialize all kv values.
**  Susan Long                          07/28/2005   cc6615
**    Reason:  Remove hardcoded fixed fund investments
**  Susan Long                          09/14/2005   cc8862
**    Reason:  20% fixed interest transfer restriction corrections.  Basically redid
**             much of the logic in this code.
**  Susan Long                          11/11/2005   cc9293
**    Reason:  For conversion plans, the beginning ff balances and transfers were entered into
**             PF609/610(beginning bal) and PF611/612(transfers) for FF1 and FF2 respectively.
**             Now that GIF and LIF are being converted also, these values all need to be set
**             in only PF609 and PF611 since it will not be known which will correspond to
**             which fund.  ONE-CC9293 will need to be run before this is moved and will also
**             need to be run after each conversion week-end run.
**  Susan Long                          12/14/05     possible production problem
**             Set max loops
**  Judy Stewart                        03/01/2006   cc9709
**    Reason:  Omniplus 5.5 Upgrade.
**  Susan Long                         05/24/2006    CC8898
**   Reason: Make functionality chgs to update/improve code per Sungard suggestions.
**           Remove code about 'true' percentage because if amt type=1, it is now 'true' percentage
**           removing the dollar amount from the properties tab to the 'true' dollar field.
**           Put IFs around the IVICOBJ_GETs
**           Remove BATCUT_FETCHCURR() not needed since it is done in the calling calculator
**  Susan Huppert                      07/16/2007    CC11784
**   Reason: Correct fixed fund restrictions to allow different transfer amounts
**  Judy Stewart                       10/08/2009    WMS1519B
**   Reason: Omniplus 5.8 Upgrade
**  Danny Hagans                       06/20/2010    WMS4463
**   Reason: SDB % transfers
**************************************************************************************
WK028 = KV002;                     /* Hold on to trade date for resetting    */
TX004 = "";
WK029 = 0;
TX067="N";
SD080=999999999;
*Sum of participants funds and funds of the transfer are current market value
EACH@***;
   WF004=0;
   WF005=0;
   WF006=0;
   WF008=.01; /* WMS1519B */
   IF (IC008<>"LN") AND (IC008<>"LD") AND (IC008<>"XX"); /* WMS1519B */
      WK017=+PA999;    /* Sum total of participants funds, not loans   */
      IF ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
         ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
         (SC602="1");
         WF004=PA999;
         WF005=PA130;
         WF006=PA100;
         WF008=0;      /* WMS1519B */
         WK029=+PA999; /*holds sum of funds involved with the transfer for prorata */
         IF FC700="F";
            TX067="Y";
         END;
      END;
   END;
ENDEACH;
TX013 = 'Y';
TX014 = 'Y';
*Don't deduct prior transfers if this is a Sweeps
* or if the total account balance of the participant > maximum fund balance
IF TX012 = 'SWP';
   TX013 = 'N';
   TX014 = 'N';
END;
IF ((WK017 > PMPMOBJ_NUMDE(411)) AND (TX012 = 'IFT'));
   TX013 = 'N';
   TX014 = 'N';
END;
*determine if a conversion plan
WK050 = OCDATE_BUMP(DATE:WK018 DAY:1);  /* current pye beginning */
WK053 = OCDATE_BUMP(DATE:WK018 YEAR:1); /* current pye */
WK051 = PMPMOBJ_NUMDE(345);             /* Conversion date*/
IF (WK051 > WK050) AND (WK051 < WK053); /* if conv is between beg & end pye */
   WK054=1;  /* this plan is a conversion plan in the conversion year */
ELSE;
   WK054=0;
END;
************************************************************************************************
*if harris trust and there is a fixed from fund, need to do the 20% transfer check
* CC8898 to check specifically for H1 instead of the old code that said <> H2 which would
* get all the plans coded with PM723=any other code like N1
IF (TX067="Y") AND (PMPMOBJ_DE(723) = "H1"); /* if there is a fixed from fund */
   IF (PMPMOBJ_DE(662) = "B");
      KV002=WK018;                /* based on beginning of contract year */
   ELSE;
      KV002=WK028;                /* based on the trade date */
   END;
   EACH@***;
      TX003 = OCTEXT_SUB(FC026 1 2);   /*  investment id */
      IF FC700="F";
         IF (TX004="") OR (TX004=TX003); /* FF1 */
            TX004=TX003;          /* Holds first FF */
            IF WK054=1;           /* plan in conversion year, get from PF609 */
               WK015=+PF609;
            ELSE;
               IF IC322=100;      /* cc11784 if IC322=100, transfer from trade date bal */
                  KV002=WK028;
               END;
               WK015=+PA999;         /* first FF balance */
            END;
         ELSE;                    /* must be the second fixed fund */
            TX044=TX003;          /* TX044 will hold the FF2 */
            IF WK054=1;           /* plan in conversion year, get from PF609 */
               WK016=+PF609;
            ELSE;
               IF IC322=100;      /* cc11784 if IC322=100, transfer from trade date bal */
                  KV002=WK028;
               END;
               WK016=+PA999;         /* second FF balance */
            END;
         END;
      END;
   ENDEACH;
***This will cause the current market value to be used, and no deducts
   IF (((TX012='IFT') OR (TX012='REB')) AND (WK015 < PMPMOBJ_NUMDE(412)));
      TX013 = 'N';           /* CC4093: Added REB to above */
   END;
   IF (((TX012='IFT') OR (TX012='REB')) AND (WK016 < PMPMOBJ_NUMDE(412)));
      TX014 = 'N';           /* CC4093: Added REB to above */
   END;
***not sure we should do this?????
   IF ((TX012='IFT') OR (TX012='REB')) AND     /* if IFT or REB transfer and  */
      ((PMPMOBJ_DE(723)="H1") AND (WK054=1));  /*  if harris trust and a conversion pln in yr*/
      TX013='Y';
      TX014='Y';
   END;
*  get max available to transfer
   IF TX004<>"";
      IF IVICOBJ_GET(PLAN:SD001 FUNDIV:TX004);  /* get investment control for FF1 */
         WK045=WK015*(OCTEXT_TONUM(IVICOBJ_DE(322))/100); /* usually 20% value in IC322 */
         IF IVICOBJ_NUMDE(322)=100;    /* cc11784 if IC322=100,do not deduct prior xfers  */
            TX013="N";
         END;
      END;
   END;
   IF TX044<>"";
      IF IVICOBJ_GET(PLAN:SD001 FUNDIV:TX044);  /* get investment control for FF2 */
         WK046=WK016*(OCTEXT_TONUM(IVICOBJ_DE(322))/100);
         IF IVICOBJ_NUMDE(322)=100;    /* cc11784 if IC322=100,do not deduct prior xfers  */
            TX014="N";
         END;
      END;
   END;
*  Need to deduct prior transfer dollars based upon contract year
   IF (TX013 = 'Y') OR (TX014 = 'Y');
      IF PMPMOBJ_DE(662) = "B";
         WK027=OCDATE_ADDDAYS(WK018 1);  /* get one day after start of PYE */
      ELSE;
         WK027=OCDATE_BUMP(WK028 YEAR:-1 DAY:1); /* go back 1 year plus 1 day from TD */
      END;
      KV002=WK028;                     /* trade date */
      HIBROBJ_VIEW(PLAN:SD001 PARTID:SD002 TRADEDATELO:WK027 TRADEDATEHI:WK028
                   TRAN:'381' ACTIVITY:021 STATUS:'ACTIVE');
      LOOP WHILE HIBROBJ_NEXT();
         IF FNFCOBJ_GET(PLAN:SD001 FUND:HIBROBJ_DE(100));
            IF FNFCOBJ_DE(700)="F";
               IF (TX013="Y") AND (OCTEXT_SUB(HIBROBJ_DE(100) 1 2)=TX004) AND /* FF1 */
                  (OCFIND(HIBROBJ_DE(105) 'I' 'C' 'T')=0);
                  WK045=WK045-HIBROBJ_NUMDE(110);
               END;
               IF (TX014="Y") AND (OCTEXT_SUB(HIBROBJ_DE(100) 1 2)=TX044) AND /* FF2 */
                  (OCFIND(HIBROBJ_DE(105) 'I' 'C' 'T')=0);
                  WK046=WK046-HIBROBJ_NUMDE(110);
               END;
            END;
         END;
      ENDLOOP;
      IF WK054=1;    /* if conv plan in conv year, subtract the maas xfer amts also */
         EACH@***;
            IF IC008=TX004;        /* FF1 */
               WK045=WK045-PF611;  /* also subtract out the transfers from Maas */
            ELSEIF IC008=TX044;    /* FF2 */
               WK046=WK046-PF611;  /* also subtract out the transfers from Maas */
            END;
         ENDEACH;
      END;
   END;
   INITKV;
   KV002=WK028;          /* Reset KV002 to trade date  */
   EACH@***;
      IF (FC700="F") AND
         ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
         ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
         (SC602="1");
         IF (IC008=TX004) AND
            (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
            IF OCTEXT_TONUM(FC063) < 2;
               WK035=WK035+PA999;
            END;
         ELSEIF (IC008=TX044) AND
            (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
            IF OCTEXT_TONUM(FC063) < 2;
               WK036=WK036+PA999;
            END;
         END;
      END;
   ENDEACH;
*  prorate the fixed from funds
   EACH@***;
      IF (FC700="F") AND
         ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
         ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
         (SC602="1");
         IF ((WK017 <= PMPMOBJ_NUMDE(411)) AND (TX012 = 'IFT')) OR (TX012 >< 'IFT');
            IF (IC008=TX004) AND  /* FF1 */
               (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
               IF ((TX012='IFT') AND (WK015 >= PMPMOBJ_NUMDE(412))) OR (TX012 >< 'IFT');
                  IF FC026=BATCUT_TRANFUND();  /* if from is IIS */
                     WF004=WK045;  /* the FF1 20% minus xfers */
                  ELSE;
                     WF004 = (PA999/WK035) * (WK045);
                     WK035 = WK035 - PA999;
                     WK045 = WK045 - WF004;
                  END;
               END;
            ELSEIF (IC008=TX044) AND /* FF2 */
               (OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***");
               IF ((TX012='IFT') AND (WK016>=PMPMOBJ_NUMDE(412))) OR (TX012 >< 'IFT');
                  IF FC026=BATCUT_TRANFUND(); /* if from is IIS */
                     WF004=WK046;  /* the FF1 20% minus xfers */
                  ELSE;
                     WF004 = (PA999/WK036) * (WK046);
                     WK036 = WK036 - PA999;
                     WK046 = WK046 - WF004;
                  END;
               END;
            END;
         END;
      END;
   ENDEACH;
END;                       /* CC5585 end of if not a harris trust plan */
***********************************************************************************************
*the following is now done for all the from funds
IF TX012='REB';
   EACH@***;
      IF WF004 > PA999;
         WF004 = PA999;
      END;
   ENDEACH;
END;
EACH@***;
   IF ((OCTEXT_MATCHFILTER(FC026 FILTER:BATCUT_TRANFUND())) OR (BATCUT_TRANFUND()="***")) AND
      ((SC007=OCSUB(BATCUT_TRANFUND() 3 1)) OR (OCSUB(BATCUT_TRANFUND() 3 1)='*')) AND
      (SC602="1");
      WK068=(PA999)*(WK019/WK029);  /*acct bal times (xfer amt divided by sum of from funds*/
      IF WF004>WK068;
         WF004=WK068;
      END;
      IF WF004<.01;
         WF004=0;
         WF008=.01; /* WMS1519B */
      END;
      IF (BATCUT_NUMVTDE(430)=0) AND (BATCUT_DE(074)="1"); /* true percentage */
        WK069=BATCUT_NUMDE(076)*PA999; /* WMS1519B - percentage times acct bal of from fund */
        IF WF004<WK069; /* WMS1519B */
           WF004=WF004/BATCUT_NUMDE(076); /* WMS1519B - set limit so when Omni takes the %, it will */
        ELSE;               /* WMS1519B  figure the correct limit */
*           IF TX033<>'Y';    /* WMS1519B  only if not an SD transfer */
             WF004=PA999;    /* if limit exceeds the percent, set so Omni will get the true % */
*           END;              /* WMS1519B */
        END;
     END;
   END;
ENDEACH;
@  ���p���yJ  U P6WAB P  �Z   T  W 
 H TRN-FFMINMAX I Initial Load
X >YxsS 	Baseline    S change to v3.0    ^ Production    ^ Development    @  ���S���\