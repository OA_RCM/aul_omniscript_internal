� Polytron VCS logfile A  @      ����B    O p6wab C >YpcD >YxTE Initial revision.
F� 3�i� 3�  ********************************************************************************
********************************************************************************
**
**    TEXT FILE NAME:  PLN-MERGER
**      Calculator for mergers to be run in process step M2 to extract the market
**      values sold in the previous transaction.  Looks at T395 BR records with the current
**      days trade date.
**      Creates a folder in the 'surviving' plan called MERGER.T301 which will be run in
**      an M3 process step.  The 301 transactions would also have a 'P' on the usage code 4
**      on the properties tab for the general ledger interface.
**      NOTE: another trade date may be entered in the omniscript tab on the T966 if an
**            'override' is necessary using WK001=[date in yyyymmdd format];
**            Be sure to add the colon at the end of the line example: WK001=20030508;
**      NOTE: The T966 vba will have a pushbutton for this merger process.  If the user
**            indicates in this vba a one for one merge for funds, then the calculator will
**            create a 301 for each fund from the history 395 record.  However, if the user
**            has indicated that there is not a one for one fund merge, the omniscript will
**            have various TX fields with the 'from' investment and the correcsponding 'to'
**            investment.  For example: TX010="A4";TX011="E8";
**                                      This would be that all 395 funds for investment "A4"
**                                      would create a 301 for "E8" + source from the 395.
**
**    VARIABLE LIST:
**      TX001='to' plan, input into the T966 Omniscript tab
**      TX002=T395 partid
**      TX003=T395 fundid
**      TX004=T395 cash
**      TX005=T395 shares
**
**    OUTPUT:
**      Creates a MERGER.T301 folder in the 'surviving' plan and the participants 301
**      transaction for each fund
**
**    TRANSACTION WHICH CALL'S THE CALCULATOR: T966 in M2 process step
**
**    CREATED BY                           DATE         SMR
**    ==============================     ========     ========
**    Susan Long                         04/04/2003   cc4415
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER                           DATE         SMR
**    ==============================     ========     ========
**    Susan Long                         12/01/04     cc7499
**     Reason:  Using TX030 when there were more than 10 investment mergers would cause
**               the IT000 to mess up on the 11th investment.  Removed the TX030 folder
**               name and inserted it directly on the BATIUT_TOVTRAN
**    Susan Long                         12/22/2004   cc7528
**     Reason:  New option for bucket to bucket mergers.
**       If chosen on the vba, then for each fixed fund, create a T021 for the fixed buckets
**       instead of using the T301 COOV.
**       Also skip reversal histroy records.
**       Write to both the 301 and 021 transaction the contribution amount found in the
**       PF110 record.  For the 021, only write this to the first 021 record for each fund.
**       Also redesigned how to read the MERGERFILE.  Instead of putting all
**       the IT000 AND IT001 text fields, just read the file when needed.  Also updated using
**       new code OCTEXT_SUB instead of SD060.
**    Judy Stewart                       03/01/2006   cc9709
**     Reason:  Omniplus 5.5 Upgrade
**    Judy Stewart                       02/26/2006   cc10698
**     Reason:  Support for previous high participant level commissions
********************************************************************************
PARM SECTION.
OR_RPT_MODE=CALC
TEXT SECTION.
CALC SECTION.
BATCUT_FETCHCURR();                           /* CC10698 - Retrieve current transaction data */
TX007 = BATCUT_VTDE(581);                     /* CC10698 - transaction std usage code 1 on properties tab */
IF TX007='M';
  TX008=' ';                                  /* CC10698 - mergers should have std usage code 1 eq blank */
ELSEIF TX007='C';
  TX008='C';                                  /* CC10698 - prod conversions should have std usage code 1 eq C */
ELSEIF (TX007='S') OR (TX007='P');
  TX008='S';                                  /* CC10698 - spinoffs & portabilities should have std usage code 1 eq S */
END;
WK014 = BATCUT_TRANDATE();                    /* CC10698 - transaction trade date */
** WK014 = BATCUT_NUMVTDE(025);               /* CC10698 - VTRAN transaction trade date */
OCDATA_CODESTROY(NAME:'MERGER.CONTAINER');
IF WK001=0;                                   /* if no value in wk001 in omniscript */
   WK001=SD003;                               /*  use the current system date */
END;
IF WK009=1;                                   /* CC10698 - Generate T613 to update PL818 on */
   IF PTPHOBJ_FIRST(PLAN:SD001) = 1;          /* CC10698 - Retrieve first partid on plan, create T613 when processing this partid */
     TX010=PTPHOBJ_DE(007);
   END;
   IF (TX010=SD002);
     PERFORM 'CREATE.T613';                     /* on surviving plan with PL818 from old plan  */
   END;
END;                                          /* VBA passes WK009=1 flag for this */
WK005=0;WK006=0;
WK011=0;                                      /* initialize the bucket container count */
IF WK007=1;  /* bucket to bucket */
*create a container with all the 395/090 information needed for the overrides
   HIBROBJ_VIEW(PLAN:SD001 PARTID:SD002 TRADEDATE:WK001 TRAN:'395' ACTIVITY:090);
   LOOP WHILE HIBROBJ_NEXT();
     IF HIBROBJ_DE(220)="";
      IF OCDATA_COGET(NAME:'MERGER.CONTAINER')=0;
         OCDATA_COCREATE(NAME:'MERGER.CONTAINER' KEYLENG:14 TX10S:1 NUMS:4);
      END;
      TX030=HIBROBJ_DE(100)+OCFMT((HIBROBJ_NUMDE(133)) 'Z,11V0'); /* 9709 - fund + date */
      OCDATA_ITEMGET(NAME:'MERGER.CONTAINER' KEY:TX030);
      OCDATA_SETDE(1 VALUE:HIBROBJ_NUMDE(110));   /* cash */
      OCDATA_SETDE(2 VALUE:HIBROBJ_NUMDE(121));   /* units */
      OCDATA_ITEMUPDATE();
      WK011=+1;
     END;
   ENDLOOP;
END;
IF WK005=0;                                   /* no errors */
   HIBROBJ_VIEW(PLAN:SD001 PARTID:SD002 TRADEDATE:WK001 TRAN:'395' ACTIVITY:059);
   LOOP WHILE HIBROBJ_NEXT();                        /* get all 395 tranactions for date */
      IF HIBROBJ_DE(220)="";                        /* not a reversal */
         TX002=HIBROBJ_DE(007);                         /* partid */
         TX003=HIBROBJ_DE(100);                         /* fundid */
         TX004="";                                      /* initialize */
         IF (TX009="@@");                               /* investments from mergerfile */
            TXTXOBJ_VIEW(PLAN:SD001 FILENAME:'MERGERFILE');
            LOOP WHILE TXTXOBJ_NEXT();
               TX100=TXTXOBJ_DATA();
               IF OCTEXT_SUB(TX100 1 1)="*";
               ELSE;
                  IF OCTEXT_SUB(TX100 1 2)=OCTEXT_SUB(TX003 1 2);
                     TX004=OCTEXT_SUB(TX100 4 2)+OCTEXT_SUB(TX003 3 1);  /* read the to fund */
                  ENDEXIT;
               END;
            ENDLOOP;
         END;
         SD031=10;
         IF (TX009<>"@@") AND (TX009<>"**");         /* investments were entered on the vba */
            LOOP UNTIL (SD031=20);                   /* get values from the tx fields on the */
               IF OCTEXT_SUB(TX003 1 2)=IT000;       /* if investment from br = tx value */
                  TX004=IT001+OCTEXT_SUB(TX003 3 1); /* set the 301 fund = 'to' tx value */
               ENDEXIT;
               SD031=SD031+2;                        /* add 2 to the it value */
            ENDLOOP;
         END;
         IF TX004="";                                /* for one to one fund merger */
            TX004=TX003;                             /* the 301 record with the br fund */
         END;
         IF WK007=0;                                 /* not bucket to bucket merger */
            PERFORM "ROUTINE.REGULAR";               /* create fund to fund 301 transactions */
         ELSE;
            PERFORM "ROUTINE.FIXEDBUCKET";        /* for ff create bucket to bucket overrides*/
         END;
      END;
   ENDLOOP;
END;
OCDATA_CODESTROY(NAME:'MERGER.CONTAINER');
************************************************************************************************
* routines
************************************************************************************************
ROUTINE "ROUTINE.REGULAR";
WK002=HIBROBJ_NUMDE(110);                      /* cash */
WK003=HIBROBJ_NUMDE(121);                      /* shares */
WK010=HIBROBJ_NUMDE(150);                      /* other cash */
*WK004=PTPFOBJ_GET(PLAN:SD001 PARTID:SD002 FUND:TX003);
*IF WK004=1;
*   WK010=PTPFOBJ_DE(110);  /* contributions */
*ELSE;
*   RPLOG_ERROR(MSG:('AUL: Participant '+OCTEXT_SUB(SD002 1 9) + ' Fund ' + TX003 + ' not found'));
*END;
BATIUT_INIT(PLAN:TX001 PARTID:TX002 TRAN:'301' DATE:WK001 FUND:TX004);
BATIUT_SETDE(050 'EXT');   /* from */
BATIUT_SETDE(080 'PXFI');  /* to */
BATIUT_SETDE(110 WK002);   /* cash amount */
BATIUT_SETDE(160 WK010);   /* contributions */
BATIUT_SETDE(260 'P');
BATIUT_SETVTDE(581 TX008); /* CC10698 - specific std usage code 1 on properties tab */
BATIUT_SETVTDE(440 SD001); /* CC10698 - set OLD PLAN for use in commission system */
BATIUT_ADDTRAN();
BATIUT_TOVTRAN(FILEID:'MERGER' STEP:'M5');
GOBACK;
***********************************************************************
ROUTINE "ROUTINE.FIXEDBUCKET";
*need to get FC700 to see if this is a fixed fund
WK008=FNFCOBJ_GET(PLAN:SD001 FUND:TX003);
IF WK008=1;
   IF FNFCOBJ_DE(700)="F";
      PERFORM "ROUTINE.FF";
   ELSE;
      PERFORM "ROUTINE.REGULAR";
   END;
ELSE;
   RPLOG_ERROR(MSG:('AUL: Cannot find fund control for ' + TX003));
END;
GOBACK;
***********************************************************************
ROUTINE "ROUTINE.FF";
*read the container for the fund
WK022=0;
WK023=0;
WK050=55;       /* first de number for the coov tab screen */
WK002=OCDATA_CoGet(NAME:'MERGER.CONTAINER');
IF WK002=1;
   OCDATA_CoView(NAME:'MERGER.CONTAINER');
   LOOP WHILE OCDATA_CoNext();
      OCDATA_ITEMVIEW();
      LOOP WHILE OCDATA_ITEMNEXT();
         TX006=OCDATA_ITEMKEY();          /* key of the container ie. I2A19960201622 */
         IF TX003=OCTEXT_SUB(TX006 1 3);  /* fundid in key */
            WK020=OCDATA_DENUM(1);        /* cash amount */
            WK021=OCDATA_DENUM(2);        /* units */
            TX012=OCTEXT_SUB(TX006 1 3);
            OCSHOW(CTR:TX012 CTR:TX013);
            IF TX012<>TX013;
               TX013=TX012;
               WK025=1;
               WK013=HIBROBJ_NUMDE(150);  /* other cash from the 395 059 record */
*               WK012=PTPFOBJ_GET(PLAN:SD001 PARTID:SD002 FUND:TX003);
*               IF WK012=1;
*                  WK013=PTPFOBJ_DE(110);     /* fund contritutions */
*               ELSE;
*                  RPLOG_ERROR(MSG:('AUL: Participant '+OCTEXT_SUB(SD002 1 9) + ' Fund ' + TX003 + ' not found'));
*               END;
            END;
            BATIUT_INIT(PLAN:TX001 PARTID:TX002 TRAN:'021' DATE:WK001 FUND:TX004);
            BATIUT_SETDE(050 'RECEIPT UNITS');
            WK099=OCTEXT_TONUM(OCTEXT_SUB(TX006 4 11) 1 11);
            BATIUT_SETDE(080 WK099);
            BATIUT_SETDE(100 WK021);
            BATIUT_SETDE(090 WK020);
            IF WK025=1;
               BATIUT_SETDE(095 WK013);
               WK025=+1;
            ELSE;
               BATIUT_SETDE(095 0);
            END;
            BATIUT_SETVTDE(581 TX008); /* CC10698 - specific std usage code 1 on properties tab */
            BATIUT_SETVTDE(440 SD001); /* CC10698 - set OLD PLAN for use in commission system */
            BATIUT_ADDTRAN();
            BATIUT_TOVTRAN(FILEID:'MERGER' STEP:'M5');
         END;
      ENDLOOP;
   ENDLOOP;
END;
GOBACK;
***********************************************************************
ROUTINE "CREATE.T613";
*need to update PL818 on surviving plan when WK009=1 and processing first partid
  TX014=OCFMT_DATE3(SD003)+'.M613';
  PLPLOBJ_VIEW(PLAN:SD001);        /* CC10698 - Retreive PL818 on from plan */
  LOOP WHILE PLPLOBJ_NEXT();
     WK015=PLPLOBJ_NUMDE(818);     /* plan date of first contribution */
  ENDLOOP;
  IF WK015 > 0;
    BATIUT_INIT(PLAN:TX001 PARTID:'000000000' TRAN:'613' DATE:SD003);
    BATIUT_SETDE(050 '818');         /* DE PL818 */
    TX018=OCFMT_DATE3(WK015);        /* TEXT PL818 for transaction update */
    BATIUT_SETDE(055 TX018);         /* old plan PL818 */
    BATIUT_SETDE(057 '1');           /* function=report all changes to maint log */
    BATIUT_ADDTRAN();
    WK016 = BATIUT_TOVTRAN(FILEID:TX014 SYSTEM:'PROD' STEP:'M5');
    IF WK016 = 0;
      RPLOG_ERROR(MSG:('AUL: T613 - PLAN MAINT FOR PL818 - Error creating folder '+TX014));
    ELSE;
      RPLOG_INFO(MSG:('AUL: T613 - PLAN MAINT FOR PL818 - Added to folder '+TX014));
    END;
  ELSE;
    RPLOG_ERROR(MSG:('AUL: T613 - PLAN MAINT NOT GENERATED - PL818 = ZERO ON PLAN '+SD001));
  END;

e@  ������B    O P6WAB C >YpcD >Y}�E change to v3.0
M             G� 3�********************************************************************************
********************************************************************************
**
**    TEXT FILE NAME:  PLN-MERGER
**      Calculator for mergers to be run in process step M2 to extract the market
**      values sold in the previous transaction.  Looks at T395 BR records with the current
**      days trade date.
**      Creates a folder in the 'surviving' plan called MERGER.T301 which will be run in
**      an M3 process step.  The 301 transactions would also have a 'P' on the usage code 4
**      on the properties tab for the general ledger interface.
**      NOTE: another trade date may be entered in the omniscript tab on the T966 if an
**            'override' is necessary using WK001=[date in yyyymmdd format];
**            Be sure to add the colon at the end of the line example: WK001=20030508;
**      NOTE: The T966 vba will have a pushbutton for this merger process.  If the user
**            indicates in this vba a one for one merge for funds, then the calculator will
**            create a 301 for each fund from the history 395 record.  However, if the user
**            has indicated that there is not a one for one fund merge, the omniscript will
**            have various TX fields with the 'from' investment and the correcsponding 'to'
**            investment.  For example: TX010="A4";TX011="E8";
**                                      This would be that all 395 funds for investment "A4"
**                                      would create a 301 for "E8" + source from the 395.
**
**    VARIABLE LIST:
**      TX001='to' plan, input into the T966 Omniscript tab
**      TX002=T395 partid
**      TX003=T395 fundid
**      TX004=T395 cash
**      TX005=T395 shares
**
**    OUTPUT:
**      Creates a MERGER.T301 folder in the 'surviving' plan and the participants 301
**      transaction for each fund
**
**    TRANSACTION WHICH CALL'S THE CALCULATOR: T966 in M2 process step
**
**    CREATED BY                           DATE         SMR
**    ==============================     ========     ========
**    Susan Long                         04/04/2003   cc4415
**
**    MODIFICATION HISTORY:
**
**    PROGRAMMER                           DATE         SMR
**    ==============================     ========     ========
**    Susan Long                         12/01/04     cc7499
**     Reason:  Using TX030 when there were more than 10 investment mergers would cause
**               the IT000 to mess up on the 11th investment.  Removed the TX030 folder
**               name and inserted it directly on the BATIUT_TOVTRAN
**    Susan Long                         12/22/2004   cc7528
**     Reason:  New option for bucket to bucket mergers.
**       If chosen on the vba, then for each fixed fund, create a T021 for the fixed buckets
**       instead of using the T301 COOV.
**       Also skip reversal histroy records.
**       Write to both the 301 and 021 transaction the contribution amount found in the
**       PF110 record.  For the 021, only write this to the first 021 record for each fund.
**       Also redesigned how to read the MERGERFILE.  Instead of putting all
**       the IT000 AND IT001 text fields, just read the file when needed.  Also updated using
**       new code OCTEXT_SUB instead of SD060.
**    Judy Stewart                       03/01/2006   cc9709
**     Reason:  Omniplus 5.5 Upgrade
**    Judy Stewart                       02/26/2006   cc10698
**     Reason:  Support for previous high participant level commissions
********************************************************************************
PARM SECTION.
OR_RPT_MODE=CALC
TEXT SECTION.
CALC SECTION.
BATCUT_FETCHCURR();                           /* CC10698 - Retrieve current transaction data */
TX007 = BATCUT_VTDE(581);                     /* CC10698 - transaction std usage code 1 on properties tab */
IF TX007='M';
  TX008=' ';                                  /* CC10698 - mergers should have std usage code 1 eq blank */
ELSEIF TX007='C';
  TX008='C';                                  /* CC10698 - prod conversions should have std usage code 1 eq C */
ELSEIF (TX007='S') OR (TX007='P');
  TX008='S';                                  /* CC10698 - spinoffs & portabilities should have std usage code 1 eq S */
END;
WK014 = BATCUT_TRANDATE();                    /* CC10698 - transaction trade date */
** WK014 = BATCUT_NUMVTDE(025);               /* CC10698 - VTRAN transaction trade date */
OCDATA_CODESTROY(NAME:'MERGER.CONTAINER');
IF WK001=0;                                   /* if no value in wk001 in omniscript */
   WK001=SD003;                               /*  use the current system date */
END;
IF WK009=1;                                   /* CC10698 - Generate T613 to update PL818 on */
   IF PTPHOBJ_FIRST(PLAN:SD001) = 1;          /* CC10698 - Retrieve first partid on plan, create T613 when processing this partid */
     TX010=PTPHOBJ_DE(007);
   END;
   IF (TX010=SD002);
     PERFORM 'CREATE.T613';                     /* on surviving plan with PL818 from old plan  */
   END;
END;                                          /* VBA passes WK009=1 flag for this */
WK005=0;WK006=0;
WK011=0;                                      /* initialize the bucket container count */
IF WK007=1;  /* bucket to bucket */
*create a container with all the 395/090 information needed for the overrides
   HIBROBJ_VIEW(PLAN:SD001 PARTID:SD002 TRADEDATE:WK001 TRAN:'395' ACTIVITY:090);
   LOOP WHILE HIBROBJ_NEXT();
     IF HIBROBJ_DE(220)="";
      IF OCDATA_COGET(NAME:'MERGER.CONTAINER')=0;
         OCDATA_COCREATE(NAME:'MERGER.CONTAINER' KEYLENG:14 TX10S:1 NUMS:4);
      END;
      TX030=HIBROBJ_DE(100)+OCFMT((HIBROBJ_NUMDE(133)) 'Z,11V0'); /* 9709 - fund + date */
      OCDATA_ITEMGET(NAME:'MERGER.CONTAINER' KEY:TX030);
      OCDATA_SETDE(1 VALUE:HIBROBJ_NUMDE(110));   /* cash */
      OCDATA_SETDE(2 VALUE:HIBROBJ_NUMDE(121));   /* units */
      OCDATA_ITEMUPDATE();
      WK011=+1;
     END;
   ENDLOOP;
END;
IF WK005=0;                                   /* no errors */
   HIBROBJ_VIEW(PLAN:SD001 PARTID:SD002 TRADEDATE:WK001 TRAN:'395' ACTIVITY:059);
   LOOP WHILE HIBROBJ_NEXT();                        /* get all 395 tranactions for date */
      IF HIBROBJ_DE(220)="";                        /* not a reversal */
         TX002=HIBROBJ_DE(007);                         /* partid */
         TX003=HIBROBJ_DE(100);                         /* fundid */
         TX004="";                                      /* initialize */
         IF (TX009="@@");                               /* investments from mergerfile */
            TXTXOBJ_VIEW(PLAN:SD001 FILENAME:'MERGERFILE');
            LOOP WHILE TXTXOBJ_NEXT();
               TX100=TXTXOBJ_DATA();
               IF OCTEXT_SUB(TX100 1 1)="*";
               ELSE;
                  IF OCTEXT_SUB(TX100 1 2)=OCTEXT_SUB(TX003 1 2);
                     TX004=OCTEXT_SUB(TX100 4 2)+OCTEXT_SUB(TX003 3 1);  /* read the to fund */
                  ENDEXIT;
               END;
            ENDLOOP;
         END;
         SD031=10;
         IF (TX009<>"@@") AND (TX009<>"**");         /* investments were entered on the vba */
            LOOP UNTIL (SD031=20);                   /* get values from the tx fields on the */
               IF OCTEXT_SUB(TX003 1 2)=IT000;       /* if investment from br = tx value */
                  TX004=IT001+OCTEXT_SUB(TX003 3 1); /* set the 301 fund = 'to' tx value */
               ENDEXIT;
               SD031=SD031+2;                        /* add 2 to the it value */
            ENDLOOP;
         END;
         IF TX004="";                                /* for one to one fund merger */
            TX004=TX003;                             /* the 301 record with the br fund */
         END;
         IF WK007=0;                                 /* not bucket to bucket merger */
            PERFORM "ROUTINE.REGULAR";               /* create fund to fund 301 transactions */
         ELSE;
            PERFORM "ROUTINE.FIXEDBUCKET";        /* for ff create bucket to bucket overrides*/
         END;
      END;
   ENDLOOP;
END;
OCDATA_CODESTROY(NAME:'MERGER.CONTAINER');
************************************************************************************************
* routines
************************************************************************************************
ROUTINE "ROUTINE.REGULAR";
WK002=HIBROBJ_NUMDE(110);                      /* cash */
WK003=HIBROBJ_NUMDE(121);                      /* shares */
WK010=HIBROBJ_NUMDE(150);                      /* other cash */
*WK004=PTPFOBJ_GET(PLAN:SD001 PARTID:SD002 FUND:TX003);
*IF WK004=1;
*   WK010=PTPFOBJ_DE(110);  /* contributions */
*ELSE;
*   RPLOG_ERROR(MSG:('AUL: Participant '+OCTEXT_SUB(SD002 1 9) + ' Fund ' + TX003 + ' not found'));
*END;
BATIUT_INIT(PLAN:TX001 PARTID:TX002 TRAN:'301' DATE:WK001 FUND:TX004);
BATIUT_SETDE(050 'EXT');   /* from */
BATIUT_SETDE(080 'PXFI');  /* to */
BATIUT_SETDE(110 WK002);   /* cash amount */
BATIUT_SETDE(160 WK010);   /* contributions */
BATIUT_SETDE(260 'P');
BATIUT_SETVTDE(581 TX008); /* CC10698 - specific std usage code 1 on properties tab */
BATIUT_SETVTDE(440 SD001); /* CC10698 - set OLD PLAN for use in commission system */
BATIUT_ADDTRAN();
BATIUT_TOVTRAN(FILEID:'MERGER' STEP:'M5');
GOBACK;
***********************************************************************
ROUTINE "ROUTINE.FIXEDBUCKET";
*need to get FC700 to see if this is a fixed fund
WK008=FNFCOBJ_GET(PLAN:SD001 FUND:TX003);
IF WK008=1;
   IF FNFCOBJ_DE(700)="F";
      PERFORM "ROUTINE.FF";
   ELSE;
      PERFORM "ROUTINE.REGULAR";
   END;
ELSE;
   RPLOG_ERROR(MSG:('AUL: Cannot find fund control for ' + TX003));
END;
GOBACK;
***********************************************************************
ROUTINE "ROUTINE.FF";
*read the container for the fund
WK022=0;
WK023=0;
WK050=55;       /* first de number for the coov tab screen */
WK002=OCDATA_CoGet(NAME:'MERGER.CONTAINER');
IF WK002=1;
   OCDATA_CoView(NAME:'MERGER.CONTAINER');
   LOOP WHILE OCDATA_CoNext();
      OCDATA_ITEMVIEW();
      LOOP WHILE OCDATA_ITEMNEXT();
         TX006=OCDATA_ITEMKEY();          /* key of the container ie. I2A19960201622 */
         IF TX003=OCTEXT_SUB(TX006 1 3);  /* fundid in key */
            WK020=OCDATA_DENUM(1);        /* cash amount */
            WK021=OCDATA_DENUM(2);        /* units */
            TX012=OCTEXT_SUB(TX006 1 3);
            OCSHOW(CTR:TX012 CTR:TX013);
            IF TX012<>TX013;
               TX013=TX012;
               WK025=1;
               WK013=HIBROBJ_NUMDE(150);  /* other cash from the 395 059 record */
*               WK012=PTPFOBJ_GET(PLAN:SD001 PARTID:SD002 FUND:TX003);
*               IF WK012=1;
*                  WK013=PTPFOBJ_DE(110);     /* fund contritutions */
*               ELSE;
*                  RPLOG_ERROR(MSG:('AUL: Participant '+OCTEXT_SUB(SD002 1 9) + ' Fund ' + TX003 + ' not found'));
*               END;
            END;
            BATIUT_INIT(PLAN:TX001 PARTID:TX002 TRAN:'021' DATE:WK001 FUND:TX004);
            BATIUT_SETDE(050 'RECEIPT UNITS');
            WK099=OCTEXT_TONUM(OCTEXT_SUB(TX006 4 11) 1 11);
            BATIUT_SETDE(080 WK099);
            BATIUT_SETDE(100 WK021);
            BATIUT_SETDE(090 WK020);
            IF WK025=1;
               BATIUT_SETDE(095 WK013);
               WK025=+1;
            ELSE;
               BATIUT_SETDE(095 0);
            END;
            BATIUT_SETVTDE(581 TX008); /* CC10698 - specific std usage code 1 on properties tab */
            BATIUT_SETVTDE(440 SD001); /* CC10698 - set OLD PLAN for use in commission system */
            BATIUT_ADDTRAN();
            BATIUT_TOVTRAN(FILEID:'MERGER' STEP:'M5');
         END;
      ENDLOOP;
   ENDLOOP;
END;
GOBACK;
***********************************************************************
ROUTINE "CREATE.T613";
*need to update PL818 on surviving plan when WK009=1 and processing first partid
  TX014=OCFMT_DATE3(SD003)+'.M613';
  PLPLOBJ_VIEW(PLAN:SD001);        /* CC10698 - Retreive PL818 on from plan */
  LOOP WHILE PLPLOBJ_NEXT();
     WK015=PLPLOBJ_NUMDE(818);     /* plan date of first contribution */
  ENDLOOP;
  IF WK015 > 0;
    BATIUT_INIT(PLAN:TX001 PARTID:'000000000' TRAN:'613' DATE:SD003);
    BATIUT_SETDE(050 '818');         /* DE PL818 */
    TX018=OCFMT_DATE3(WK015);        /* TEXT PL818 for transaction update */
    BATIUT_SETDE(055 TX018);         /* old plan PL818 */
    BATIUT_SETDE(057 '1');           /* function=report all changes to maint log */
    BATIUT_ADDTRAN();
    WK016 = BATIUT_TOVTRAN(FILEID:TX014 SYSTEM:'PROD' STEP:'M5');
    IF WK016 = 0;
      RPLOG_ERROR(MSG:('AUL: T613 - PLAN MAINT FOR PL818 - Error creating folder '+TX014));
    ELSE;
      RPLOG_INFO(MSG:('AUL: T613 - PLAN MAINT FOR PL818 - Added to folder '+TX014));
    END;
  ELSE;
    RPLOG_ERROR(MSG:('AUL: T613 - PLAN MAINT NOT GENERATED - PL818 = ZERO ON PLAN '+SD001));
  END;

@  ������J  U P6WAB P  �Z   T  W 
 H PLN-MERGER I Initial Load
X >YxTS 	Baseline    S change to v3.0    ^ Production    @  ���j���s