� Polytron VCS logfile A  @      ����B    O P6WAB C >YpXD >YxME Initial revision.
F� #�i� #{  ********************************************************************************************************
**
**  Text File Name:LTR-CONV-LNNUM
**  Calculator creates a letter for participants with current loans that will have a new loan number
**  due to their plan being converted from MAAS. The participant and plan sponsor will be able to
**  reference the new loan numbers when remitting future loan repayments.
**
**  Outputs:
**  External file called LTRLNNUM
**
**  Called by T966 transaction
**
**  Variable List:
**
**  TX001  Sets up external file name
**  TX002  Holds error when external file has an error opening file
**  TX003  Holds AA300 - Client's name
**  TX004  Holds error created when trying to write to the external file
**  TX005  Holds AA200 and AA210 - Contact's name
**  TX006  Holds AA310 - Client's street address
**  TX007  Holds AA320, AA325 and AA330 - Client's City, State, Zip code
**  TX025  Holds the letter's salutation - Dear "Contact's Name"
**  TX009  Holds PM486 and PM005 - Old MAAS case number and New OMNI case number
**  TX010  Holds column headings for old MAAS case number and New OMNI case number
**  TX012  Holds column headings for SSN, old MAAS loan number and new OMNI loan number
**  TX013  Holds LH007 - Participant's SSN
**  TX014  Holds LH610 and LH300 - old MAAS loan number and new OMNI loan number
**  TX015  Holds the text fields (TX013 plus TX014) that are to be written to the external file
**  TX020  Holds the Client's Zip Code AA330
**  TX021  Holds the last four digits of the Client's Zip Code AA330
**  TX022  Holds part of the client's name - AA305
**  TX028  Holds plan number
**  TX100  Holds the ASCII characters
**  TX102  Holds the character in position 2 of the barcode
**  TX104  Holds the character in position 4 of the barcode
**  TX105  Holds the character in position 5 of the barcode
**  TX107  Holds the character in position 7 of the barcode
**  TX110  Holds the barcode
**  WK001  Counter for number of lines on page so page break will occur at 60 lines
**  WK002  Counter for lines above the mailing address
**  WK100  Keeps track of the number of pages
**  WK101  Holds page number
**  WK102  Holds the character in position 2 of the barcode
**  WK105  Holds the character in position 5 of the barcode
**  WK107  Holds the character in position 7 of the barcode
**  WK110  Holds the page number and checks if it is divisible by seven
**
**  Created By                      Date           CC
**  ==========================      ==========   =======
**  Jodi Stone                      07/06/2004    6457
**
**  Modification History:
**
**  Programmer                      Date           CC
**  ==========================      ==========   =======
**  Rick Sica                       10/29/2004    7142
**     Bar code is incorrect. Page number (wk101) was not initialized for each plan
**  Susan Long                      07/29/2005    8825
**     800 number is incorrect.  Should be 800-261-9618
**  Judy Stewart                    03/01/2006    9709
**     Omniplus 5.5 Upgrade.
****************************************************************************************************
PARM SECTION.
OR_RPT_MODE=CALC
OR_DELIMITER='~'
TEXT SECTION.
CALC SECTION.
SD080=50000;
TX100="123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-.#$/+%";
TX001=OCTEXT_GETENV('POSTOUT')+'/LTRLNNUM.TXT';
IF OCFILE1_OPEN(NAME:TX001 MODE:'EXTEND')=0;
   TX002='FILE OPEN ERROR'+TX001;
END;
PLPLOBJ_VIEW(PLANLO:'A00000');
LOOP WHILE PLPLOBJ_NEXT();
   TX028=PLPLOBJ_DE(011);
   WK010 = LNLHOBJ_FIRST(PLAN:TX028);
   IF WK010=1;   /* If loan header found */
      WK050 = AAAAOBJ_GET(PLAN:TX028 ADDRESSID:('REM' + TX028) SEQNUM:001);
      IF WK050=0;
          RPLOG_ERROR(MSG:('AUL:ALTERNATE ADDRESS RECORD NOT FOUND '+ TX028));
      ELSE;
         WK101=0;                      /* Initialize page number */
         OCFILE1_WRITE('1');
         OCFILE1_WRITE(' ');           /* ADD 2 LINES ABOVE MAILING ADDRESS*/
         OCFILE1_WRITE(' ');
         TX023=' '+OCDATE_MMDDYYYY(SD003);
         OCFILE1_WRITE(TX023);
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         TX003=' '+ OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:300));  /*Client's Name*/
         IF OCFILE1_WRITE(TX003)=0;
            TX004='FILE WRITE ERROR';
         END;
         TX022=' '+ OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:305));
         OCFILE1_WRITE(TX022);
         TX005=' '+OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:200))+' '+ OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:210));  /*Contact's Name*/
         OCFILE1_WRITE(TX008);
         TX006=' '+AAAAOBJ_DE(310);          /*Client's street address*/
         OCFILE1_WRITE(TX006);
         TX020=AAAAOBJ_DE(330);              /*Client's zip code*/
         TX021=OCSUB(TX020 6 4);
         IF TX021>'0000';
            TX020=OCSUB(TX020 1 5)+'-'+TX021;
         ELSE;
            TX020=OCSUB(TX020 1 5);
         END;
         TX007=' '+OCTEXT_TRIMRIGHT(AAAAOBJ_DE(320))+' '+OCTEXT_TRIMRIGHT(AAAAOBJ_DE(325))+'  '+TX020;/*Client's City,State,Zip*/
         OCFILE1_WRITE(TX007);
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         TX025=' '+'Dear'+OCTEXT_PROPER(TX005)+':';   /* Salutation */
         OCFILE1_WRITE(TX025);
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' The enclosed report contains the new loan numbers which have been generated');
         OCFILE1_WRITE(' for your plan participants as a result of your recent system conversion.');
         OCFILE1_WRITE(' Please reference these loan numbers when remitting all future loan');
         OCFILE1_WRITE(' repayments. If you have any questions regarding this report, please contact');
         OCFILE1_WRITE(' your plan services consultant at 800-261-9618.');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         WK060=PMPMOBJ_GET(PLAN:TX028 PRODUCTID:PLPLOBJ_DE(025));
         IF WK060=0;
            RPLOG_ERROR(MSG:('AUL:PRODUCT MASTER RECORD NOT FOUND ' + TX028));
         ELSE;
           TX009='                      '+PMPMOBJ_DE(486)+'           '+PMPMOBJ_DE(005); /*OLD MAAS AND NEW OMNI CASE NUMBER*/
           TX010='                        MAAS PLAN          '+ '  OMNI PLAN ';
           OCFILE1_WRITE(TX010);
           OCFILE1_WRITE(TX009);
           OCFILE1_WRITE(' ');
           OCFILE1_WRITE(' ');
         END;
         TX012='               SSN    '+'     MAAS LOAN NUMBER '+'    OMNI LOAN NUMBER ';
         OCFILE1_WRITE(TX012);
         OCFILE1_WRITE(' ');
         WK001=42;                     /*NUMBER OF LINES FOR FIRST PART OF LETTER*/
         LNLHOBJ_VIEW(PLAN:TX028);
         LOOP WHILE LNLHOBJ_NEXT();
           IF WK001>60;              /*PAGE BREAK*/
              PERFORM 'BARCODE';
              OCFILE1_WRITE('1');
              OCFILE1_WRITE(TX012);  /*PRINT COLUMN HEADINGS ON MULTIPLE PAGES*/
              OCFILE1_WRITE(' ');
              WK001=3;
           END;
           TX013='           '+OCFMT_SSN(LNLHOBJ_DE(007));      /*PARTICIPANT'S SSN*/
           TX014='       '+LNLHOBJ_DE(610)+'              '+OCFMT(LNLHOBJ_NUMDE(300) 'Z3'); /*OLD MAAS AND NEW OMNI LOAN NUMBERS*/
           TX015=TX013+TX014;
           OCFILE1_WRITE(TX015);
           OCFILE1_WRITE(' ');
           WK001=WK001+2;
         ENDLOOP;
         PERFORM 'BARCODE';
      END;
   END;
ENDLOOP;
OCFILE1_CLOSE();
******************************************************************************
*ROUTINE 'BARCODE' to add a barcode to the external file so if a plan has
*multiple pages the inserter machine will put all of those pages in the
*same envelope
******************************************************************************
ROUTINE 'BARCODE';
WK101=WK101+1;
WK100=WK101;
IF WK100>42;                  /* Only 42 characters available in barcode, so if over, back up */
   LOOP UNTIL WK100<=42;
      WK100=WK100-43;
   ENDLOOP;
   IF WK100=0;
      WK100=1;
   END;
END;
TX104=OCSUB(TX100 WK100 1);
WK102=1;                      /* This would be a counter for each participant but report not by participant */
WK105=0;
WK107=WK100;                  /* Initialize check digit */
IF WK100=1;
   WK102=WK102+16;            /* Beginning of Set mark */
ELSE;
   WK110=OCNUM_REM(WK101 7);  /* Cannot fit more than 7 pages in envelope so ... */
   IF WK110=0;                /* If reached 7th page */
      WK105=WK105+4;          /* Set end of Set */
   END;
END;
TX102=OCSUB(TX100 WK102 1);
WK107=WK107+WK102;
IF WK105=0;
   TX105='0';
ELSE;
   TX105=OCSUB(TX100 WK105 1);
   WK107=WK107+WK105;
END;
IF WK107>42;                  /* Again only 42 characters available in barcode */
   LOOP UNTIL WK107<=42;
      WK107=WK107-43;
   ENDLOOP;
END;
IF WK107<>0;
   TX107=OCSUB(TX100 WK107 1);
ELSE;
   TX107='0';
END;
TX110=' *'+TX102+'0'+TX104+TX105+'0'+TX107+'*';
OCFILE1_WRITE(TX110);
GOBACK;
e@  ���6���?B    O P6WAB C >YpXD >Y}�E change to v3.0
M             G� #{********************************************************************************************************
**
**  Text File Name:LTR-CONV-LNNUM
**  Calculator creates a letter for participants with current loans that will have a new loan number
**  due to their plan being converted from MAAS. The participant and plan sponsor will be able to
**  reference the new loan numbers when remitting future loan repayments.
**
**  Outputs:
**  External file called LTRLNNUM
**
**  Called by T966 transaction
**
**  Variable List:
**
**  TX001  Sets up external file name
**  TX002  Holds error when external file has an error opening file
**  TX003  Holds AA300 - Client's name
**  TX004  Holds error created when trying to write to the external file
**  TX005  Holds AA200 and AA210 - Contact's name
**  TX006  Holds AA310 - Client's street address
**  TX007  Holds AA320, AA325 and AA330 - Client's City, State, Zip code
**  TX025  Holds the letter's salutation - Dear "Contact's Name"
**  TX009  Holds PM486 and PM005 - Old MAAS case number and New OMNI case number
**  TX010  Holds column headings for old MAAS case number and New OMNI case number
**  TX012  Holds column headings for SSN, old MAAS loan number and new OMNI loan number
**  TX013  Holds LH007 - Participant's SSN
**  TX014  Holds LH610 and LH300 - old MAAS loan number and new OMNI loan number
**  TX015  Holds the text fields (TX013 plus TX014) that are to be written to the external file
**  TX020  Holds the Client's Zip Code AA330
**  TX021  Holds the last four digits of the Client's Zip Code AA330
**  TX022  Holds part of the client's name - AA305
**  TX028  Holds plan number
**  TX100  Holds the ASCII characters
**  TX102  Holds the character in position 2 of the barcode
**  TX104  Holds the character in position 4 of the barcode
**  TX105  Holds the character in position 5 of the barcode
**  TX107  Holds the character in position 7 of the barcode
**  TX110  Holds the barcode
**  WK001  Counter for number of lines on page so page break will occur at 60 lines
**  WK002  Counter for lines above the mailing address
**  WK100  Keeps track of the number of pages
**  WK101  Holds page number
**  WK102  Holds the character in position 2 of the barcode
**  WK105  Holds the character in position 5 of the barcode
**  WK107  Holds the character in position 7 of the barcode
**  WK110  Holds the page number and checks if it is divisible by seven
**
**  Created By                      Date           CC
**  ==========================      ==========   =======
**  Jodi Stone                      07/06/2004    6457
**
**  Modification History:
**
**  Programmer                      Date           CC
**  ==========================      ==========   =======
**  Rick Sica                       10/29/2004    7142
**     Bar code is incorrect. Page number (wk101) was not initialized for each plan
**  Susan Long                      07/29/2005    8825
**     800 number is incorrect.  Should be 800-261-9618
**  Judy Stewart                    03/01/2006    9709
**     Omniplus 5.5 Upgrade.
****************************************************************************************************
PARM SECTION.
OR_RPT_MODE=CALC
OR_DELIMITER='~'
TEXT SECTION.
CALC SECTION.
SD080=50000;
TX100="123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-.#$/+%";
TX001=OCTEXT_GETENV('POSTOUT')+'/LTRLNNUM.TXT';
IF OCFILE1_OPEN(NAME:TX001 MODE:'EXTEND')=0;
   TX002='FILE OPEN ERROR'+TX001;
END;
PLPLOBJ_VIEW(PLANLO:'A00000');
LOOP WHILE PLPLOBJ_NEXT();
   TX028=PLPLOBJ_DE(011);
   WK010 = LNLHOBJ_FIRST(PLAN:TX028);
   IF WK010=1;   /* If loan header found */
      WK050 = AAAAOBJ_GET(PLAN:TX028 ADDRESSID:('REM' + TX028) SEQNUM:001);
      IF WK050=0;
          RPLOG_ERROR(MSG:('AUL:ALTERNATE ADDRESS RECORD NOT FOUND '+ TX028));
      ELSE;
         WK101=0;                      /* Initialize page number */
         OCFILE1_WRITE('1');
         OCFILE1_WRITE(' ');           /* ADD 2 LINES ABOVE MAILING ADDRESS*/
         OCFILE1_WRITE(' ');
         TX023=' '+OCDATE_MMDDYYYY(SD003);
         OCFILE1_WRITE(TX023);
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         TX003=' '+ OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:300));  /*Client's Name*/
         IF OCFILE1_WRITE(TX003)=0;
            TX004='FILE WRITE ERROR';
         END;
         TX022=' '+ OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:305));
         OCFILE1_WRITE(TX022);
         TX005=' '+OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:200))+' '+ OCTEXT_TRIMRIGHT(AAAAOBJ_DE(DENUM:210));  /*Contact's Name*/
         OCFILE1_WRITE(TX008);
         TX006=' '+AAAAOBJ_DE(310);          /*Client's street address*/
         OCFILE1_WRITE(TX006);
         TX020=AAAAOBJ_DE(330);              /*Client's zip code*/
         TX021=OCSUB(TX020 6 4);
         IF TX021>'0000';
            TX020=OCSUB(TX020 1 5)+'-'+TX021;
         ELSE;
            TX020=OCSUB(TX020 1 5);
         END;
         TX007=' '+OCTEXT_TRIMRIGHT(AAAAOBJ_DE(320))+' '+OCTEXT_TRIMRIGHT(AAAAOBJ_DE(325))+'  '+TX020;/*Client's City,State,Zip*/
         OCFILE1_WRITE(TX007);
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         TX025=' '+'Dear'+OCTEXT_PROPER(TX005)+':';   /* Salutation */
         OCFILE1_WRITE(TX025);
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' The enclosed report contains the new loan numbers which have been generated');
         OCFILE1_WRITE(' for your plan participants as a result of your recent system conversion.');
         OCFILE1_WRITE(' Please reference these loan numbers when remitting all future loan');
         OCFILE1_WRITE(' repayments. If you have any questions regarding this report, please contact');
         OCFILE1_WRITE(' your plan services consultant at 800-261-9618.');
         OCFILE1_WRITE(' ');
         OCFILE1_WRITE(' ');
         WK060=PMPMOBJ_GET(PLAN:TX028 PRODUCTID:PLPLOBJ_DE(025));
         IF WK060=0;
            RPLOG_ERROR(MSG:('AUL:PRODUCT MASTER RECORD NOT FOUND ' + TX028));
         ELSE;
           TX009='                      '+PMPMOBJ_DE(486)+'           '+PMPMOBJ_DE(005); /*OLD MAAS AND NEW OMNI CASE NUMBER*/
           TX010='                        MAAS PLAN          '+ '  OMNI PLAN ';
           OCFILE1_WRITE(TX010);
           OCFILE1_WRITE(TX009);
           OCFILE1_WRITE(' ');
           OCFILE1_WRITE(' ');
         END;
         TX012='               SSN    '+'     MAAS LOAN NUMBER '+'    OMNI LOAN NUMBER ';
         OCFILE1_WRITE(TX012);
         OCFILE1_WRITE(' ');
         WK001=42;                     /*NUMBER OF LINES FOR FIRST PART OF LETTER*/
         LNLHOBJ_VIEW(PLAN:TX028);
         LOOP WHILE LNLHOBJ_NEXT();
           IF WK001>60;              /*PAGE BREAK*/
              PERFORM 'BARCODE';
              OCFILE1_WRITE('1');
              OCFILE1_WRITE(TX012);  /*PRINT COLUMN HEADINGS ON MULTIPLE PAGES*/
              OCFILE1_WRITE(' ');
              WK001=3;
           END;
           TX013='           '+OCFMT_SSN(LNLHOBJ_DE(007));      /*PARTICIPANT'S SSN*/
           TX014='       '+LNLHOBJ_DE(610)+'              '+OCFMT(LNLHOBJ_NUMDE(300) 'Z3'); /*OLD MAAS AND NEW OMNI LOAN NUMBERS*/
           TX015=TX013+TX014;
           OCFILE1_WRITE(TX015);
           OCFILE1_WRITE(' ');
           WK001=WK001+2;
         ENDLOOP;
         PERFORM 'BARCODE';
      END;
   END;
ENDLOOP;
OCFILE1_CLOSE();
******************************************************************************
*ROUTINE 'BARCODE' to add a barcode to the external file so if a plan has
*multiple pages the inserter machine will put all of those pages in the
*same envelope
******************************************************************************
ROUTINE 'BARCODE';
WK101=WK101+1;
WK100=WK101;
IF WK100>42;                  /* Only 42 characters available in barcode, so if over, back up */
   LOOP UNTIL WK100<=42;
      WK100=WK100-43;
   ENDLOOP;
   IF WK100=0;
      WK100=1;
   END;
END;
TX104=OCSUB(TX100 WK100 1);
WK102=1;                      /* This would be a counter for each participant but report not by participant */
WK105=0;
WK107=WK100;                  /* Initialize check digit */
IF WK100=1;
   WK102=WK102+16;            /* Beginning of Set mark */
ELSE;
   WK110=OCNUM_REM(WK101 7);  /* Cannot fit more than 7 pages in envelope so ... */
   IF WK110=0;                /* If reached 7th page */
      WK105=WK105+4;          /* Set end of Set */
   END;
END;
TX102=OCSUB(TX100 WK102 1);
WK107=WK107+WK102;
IF WK105=0;
   TX105='0';
ELSE;
   TX105=OCSUB(TX100 WK105 1);
   WK107=WK107+WK105;
END;
IF WK107>42;                  /* Again only 42 characters available in barcode */
   LOOP UNTIL WK107<=42;
      WK107=WK107-43;
   ENDLOOP;
END;
IF WK107<>0;
   TX107=OCSUB(TX100 WK107 1);
ELSE;
   TX107='0';
END;
TX110=' *'+TX102+'0'+TX104+TX105+'0'+TX107+'*';
OCFILE1_WRITE(TX110);
GOBACK;
@  ���2���;J  U P6WAB P  �Z   T  W 
 H LTR-CONV-LNNUM I Initial Load
X >YxMS 	Baseline    S change to v3.0    ^ Production    @  ���f���o