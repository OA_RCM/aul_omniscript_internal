� Polytron VCS logfile A  @      ����B    O P6WAB C >Yp�D >Yx|E Initial revision.
F� )ni� )f  ***********************************************************************************************
**
**  Program Name : VST-CHANGEELIG   Called by: IND-RESETELIG & IND-FIXELIG
**  Plan amendments require a change to the eligibility or service control.  This identifies and
**    updates participant eligibility.  Business community requires different program names are
**    entered depending on whether all participants are to be updated (IND-FIXELIG), or just
**    those not yet eligible according to PS100 and PS110 (IND-RESETELIG).  This program allows
**    us to avoid the extra effort and risk inherent in maintaining separate programs.
**
**  Inputs:
**    UDRUNTYPE (TX001) from calling program, set to "FIXELIG" if ignoring PS100/PS110 > 0 condition
**
**  Outputs:   NEXTTI folders named [trade dt].S[trx], i.e 20070101.S801
**     T801  changes elements PH021, PH054, PS100, and/or PS110
**     T877  deletes SV record for current PH054 date, if changing
**     T966  for IND-PRIORVEST
**
**  Created By                  Date         CC
**  =====================     ========     =======
**  Sue Freas                 05/28/07      10494
**
**  Modification History:
**  Programmer                Date           CC
**  =====================     ========     ========
**  Glen McPherson            11/13/07     cc12153
**  Reason: Previous entry date service record not being deleted when change to
**          hire date makes participant ineligible.
**
**  Glen McPherson            11/15/07     cc12289
**  Reason: Limitation of the PTSO trailer of 15 entries.
**  Rick Sica                 12/31/07     12325     S877 rejecting if record already deleted
**  Judy Stewart              09/22/09     WMS1519B  Omniplus 5.8 Upgrade - Omniscripts
**  Reason: Add origination date to T877
**************************************************************************************
OCLABEL(
        UDTRADEDT:WK214         UDRUNTYPE:TX001
        UDCURRENTRYDT:WK010     UDPARTREASON:TX010
        UDPARTSTAT:WK011        UDTHISSRC:TX011
        UDHIREDT:WK012          UDTRXPARTSTAT:TX012
        UDTERMDT:WK013          UDTRXELIGDATE:TX013
        UDCURRPS100:WK014       UDCTELIG:TX014
        UDCURRPS110:WK015       UDTRXTRLR:TX015
        UDPROJELIG:WK016        UDTRXDEVALUE:TX016
        UDNEWENTRYDT:WK017      UDCLIPUPDTDE:TX017
        UDTRXDENUM:WK018
       );
UDNEWENTRYDT = 99999999;
UDTRXTRLR = "";           /* Initialize T801 trailer id */
OCCLIP_CLEAR();
PTPHOBJ_GET(PLAN:SD001 PARTID:SD002);
UDCURRENTRYDT = PTPHOBJ_NUMDE(054);  /* plan entry date */
UDTERMDT = PTPHOBJ_NUMDE(056);       /* term date */
UDPARTSTAT = PTPHOBJ_NUMDE(021);     /* part status */
UDHIREDT = PTPHOBJ_NUMDE(052);       /* hire date */
UDPARTREASON = PTPHOBJ_DE(138);      /* part reason */
IF ((UDPARTSTAT <= 29) OR (UDTERMDT = 0))
   AND (UDPARTREASON <> "E") AND (UDPARTREASON <> "F");   /* CC10494: check active participants only */
  SOSCOBJ_VIEW(PLAN:SD001);
  LOOP WHILE SOSCOBJ_NEXT();
    UDTHISSRC = SOSCOBJ_DE(007);
    IF PTPSOBJ_GET(PLAN:SD001 PARTID:SD002 SOURCE:UDTHISSRC) = 0;    /* add any missing sources */
      PTPSOBJ_INIT(PLAN:SD001 PARTID:SD002 SOURCE:UDTHISSRC);
      PTPSOBJ_ADD();
    END;
    UDCURRPS100 = PTPSOBJ_NUMDE(100);
    UDPROJELIG = UDCURRPS100;  /* CC10542 initialize UDPROJELIG */
    UDCURRPS110 = PTPSOBJ_NUMDE(110);
** If running "FIXELIG" process or PS dates not set or future dated, recalc source entry dates **
    IF (UDRUNTYPE = "FIXELIG")
        OR (UDCURRPS100 = 0) OR (UDCURRPS100 > UDTRADEDT)
        OR (UDCURRPS110 = 0) OR (UDCURRPS110 > UDTRADEDT);
      UDPROJELIG = PLELIG_PROJECTENTRY(EFFDATE:UDTRADEDT SOURCE:UDTHISSRC);
      IF UDPROJELIG <> UDCURRPS110;
        OCCLIP_ADDLINE(("PS110,"+UDTHISSRC+","+OCFMT_DATE3(UDPROJELIG)));  /* 10494: add to clip instead of cards */
      END;
      IF UDPROJELIG <> UDCURRPS100;          /* If new PS100 date calculated */
        IF UDPROJELIG <= UDTRADEDT;          /* Reset PS100 to calculated date, if not in the future */
          OCCLIP_ADDLINE(("PS100,"+UDTHISSRC+","+OCFMT_DATE3(UDPROJELIG)));  /* 10494: add to clip instead of cards */
        ELSE;                /* If PS100 not already zero and new date is in the future, set PS100 to zero */
          IF UDCURRPS100 <> 0;
            OCCLIP_ADDLINE(("PS100,"+UDTHISSRC+","+OCFMT_DATE3(0)));  /* 10494: add to clip instead of cards */
          END;
        END;
      END;
    END;  /* CC10542  END added */
** Check if source updates plan entry and store earliest of those dates **
    PLELIG_VIEW(SOURCE:UDTHISSRC);
    UDCTELIG = PLELIG_CTLVAL(KEYWORD:"UPDTPLANENTRYDATE");
    IF (UDCTELIG = "0") OR (UDCTELIG="YES");       /* cc10850 - CC6823 If the current source updates plan entry dt */
      IF (UDPROJELIG < UDNEWENTRYDT) AND (UDPROJELIG <> 0);
        UDNEWENTRYDT = UDPROJELIG;                              /* store earliest date for PH054 update */
      END;
    END;
  ENDLOOP;
** If new PS dates calculated, may need to update PH054 and PH021
  IF OCCLIP_LENG() > 0;
    IF (UDNEWENTRYDT > 0) AND (UDNEWENTRYDT <= UDTRADEDT);
      IF UDPARTSTAT = 03;    /* CC6823 If PH054 date not in the future, set PH054 & PH021 */
        UDTRXPARTSTAT = "00";
        OCCLIP_INSLINEBEFORE(("PH021,"+UDTRXPARTSTAT) POS:1);
      END;
      IF UDNEWENTRYDT <> UDCURRENTRYDT;   /* If new elig date <> current plan entry date, add to clip */
        OCCLIP_INSLINEBEFORE(("PH054,"+OCFMT_DATE3(UDNEWENTRYDT)) POS:1);
        IF UDCURRENTRYDT > 0;  /* If PH054 valued and new entry date calculated, delete SV rec for current value */
           SVSVOBJ_VIEW(PLAN:SD001 PARTID:SD002 EFFECTDATE:UDCURRENTRYDT);
           LOOP WHILE SVSVOBJ_NEXT();
              BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"877" DATE:UDHIREDT);
              BATIUT_SETDE(050 UDCURRENTRYDT);
              BATIUT_SETDE(055 SVSVOBJ_NUMDE(012));
              BATIUT_SETDE(065 "2");
              BATIUT_SETVTDE(450 UDTRADEDT);   /* WMS1519B - set origination date to avoid rejects */
              BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
              BATIUT_ADDTRAN();
              BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S877") STEP:"PT"); /* must do in PT step */
           ENDLOOP;
        END;
      END;
    ELSEIF (UDNEWENTRYDT = 0) OR (UDNEWENTRYDT > UDTRADEDT);
      IF UDPARTSTAT = 00;
        UDTRXPARTSTAT = "03";
        OCCLIP_INSLINEBEFORE(("PH021,"+UDTRXPARTSTAT) POS:1);
        SVSVOBJ_VIEW(PLAN:SD001 PARTID:SD002 EFFECTDATE:UDCURRENTRYDT);
        LOOP WHILE SVSVOBJ_NEXT();
           BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"877" DATE:UDHIREDT);
           BATIUT_SETDE(050 UDCURRENTRYDT);
           BATIUT_SETDE(055 SVSVOBJ_NUMDE(012));
           BATIUT_SETDE(065 "2");
           BATIUT_SETVTDE(450 UDTRADEDT);   /* WMS1519B - set origination date to avoid rejects */
           BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
           BATIUT_ADDTRAN();
           BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S877") STEP:"PT");   /* must do in PT step */
        ENDLOOP;
      END;
      IF UDNEWENTRYDT <> UDCURRENTRYDT; /* otherwise directly update PH054=0 so no 0 SV rec is created */
        PTPHOBJ_SETDE(054 VALUE:00000000);
        PTPHOBJ_UPDATE();
      END;
    END;
** Create T801 transaction to update PH & PS DEsfrom clipboard data
    BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"801" DATE:UDHIREDT);
     BATIUT_SETDE(050 "CHG-PART");
     BATIUT_SETVTDE(450 UDTRADEDT);   /* set origination date to avoid rejects */
    BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
    BATIUT_ADDTRAN();          /* All T801 DEs on trailers, so add main trx first */
     UDTRXDENUM = 064;
     OCCLIP_VIEW();
     LOOP WHILE OCCLIP_NEXT();
       OCCSV_SETLINE(OCCLIP_LINE()); OCCSV_VIEW();
       UDCLIPUPDTDE = OCCSV_FIELD(1);
       IF (UDCLIPUPDTDE = "PH021") OR (UDCLIPUPDTDE = "PH054");  /* PH021 & 054 on PTEN trlr */
         IF UDTRXTRLR <> "PTEN";
           UDTRXTRLR = "PTEN";
         END;
         UDTRXDEVALUE = OCTEXT_SUB(UDCLIPUPDTDE 3 3);   /* DE to update in 64 & 69 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 1;
         BATIUT_SETDE(UDTRXDENUM (OCCSV_FIELD(2)) TRLR:UDTRXTRLR);  /* New DE value in 65 & 70 */
         UDTRXDENUM =+ 4;
       ELSE;                     /* PS100 & 110 on PTSO trailer */
         IF UDTRXTRLR <> "PTSO";
           IF UDTRXTRLR = "PTEN";
             BATIUT_ADDTRLR();   /* add PTEN trailer before starting PTSO updates */
           END;
           UDTRXDENUM = 60;
           UDTRXTRLR = "PTSO";
         END;
         IF UDTRXDENUM > 354;
            BATIUT_ADDTRLR();
            BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S801") STEP:"PT");
            BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"801" DATE:UDHIREDT);
            BATIUT_SETDE(050 "CHG-PART");
            BATIUT_SETVTDE(450 UDTRADEDT);   /* set origination date to avoid rejects */
            BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
            BATIUT_ADDTRAN();          /* All T801 DEs on trailers, so add main trx first */
            UDTRXDENUM = 60;
            UDTRXTRLR = "PTSO";
         END;
         UDTRXDEVALUE = OCCSV_FIELD(2);                /* Source in DEs 60-340 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 5;
         BATIUT_SETDE(UDTRXDENUM "O" TRLR:UDTRXTRLR);  /* Use override calc option in 65-345 */
         UDTRXDENUM =+ 5;
         UDTRXDEVALUE = OCTEXT_SUB(UDCLIPUPDTDE 3 3);  /* DE to update in 70-350 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 5;
         UDTRXDEVALUE = OCCSV_FIELD(3);                /* New DE value in 75-355 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 5;
       END;
     ENDLOOP;
     BATIUT_ADDTRLR();
    BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S801") STEP:"PT");
  ELSE;
    RPLOG_INFO(PLAN:SD001 PARTID:SD002 MSG:"AUL: No eligiblity changes made for participant");
  END;
ELSE;
  RPLOG_INFO(PLAN:SD001 PARTID:SD002 MSG:"AUL: Eligibility check skipped for participant");
END;
** Create T966 for IND-PRIORVEST
BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"966" DATE:UDTRADEDT FUND:"***");
 BATIUT_SETDE(050 "IND-PRIORVEST");
 BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
BATIUT_ADDTRAN();
BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S966") STEP:"PT");
e@  ���K���TB    O P6WAB C >Yp�D >Y}�E change to v3.0
M             G� )f***********************************************************************************************
**
**  Program Name : VST-CHANGEELIG   Called by: IND-RESETELIG & IND-FIXELIG
**  Plan amendments require a change to the eligibility or service control.  This identifies and
**    updates participant eligibility.  Business community requires different program names are
**    entered depending on whether all participants are to be updated (IND-FIXELIG), or just
**    those not yet eligible according to PS100 and PS110 (IND-RESETELIG).  This program allows
**    us to avoid the extra effort and risk inherent in maintaining separate programs.
**
**  Inputs:
**    UDRUNTYPE (TX001) from calling program, set to "FIXELIG" if ignoring PS100/PS110 > 0 condition
**
**  Outputs:   NEXTTI folders named [trade dt].S[trx], i.e 20070101.S801
**     T801  changes elements PH021, PH054, PS100, and/or PS110
**     T877  deletes SV record for current PH054 date, if changing
**     T966  for IND-PRIORVEST
**
**  Created By                  Date         CC
**  =====================     ========     =======
**  Sue Freas                 05/28/07      10494
**
**  Modification History:
**  Programmer                Date           CC
**  =====================     ========     ========
**  Glen McPherson            11/13/07     cc12153
**  Reason: Previous entry date service record not being deleted when change to
**          hire date makes participant ineligible.
**
**  Glen McPherson            11/15/07     cc12289
**  Reason: Limitation of the PTSO trailer of 15 entries.
**  Rick Sica                 12/31/07     12325     S877 rejecting if record already deleted
**  Judy Stewart              09/22/09     WMS1519B  Omniplus 5.8 Upgrade - Omniscripts
**  Reason: Add origination date to T877
**************************************************************************************
OCLABEL(
        UDTRADEDT:WK214         UDRUNTYPE:TX001
        UDCURRENTRYDT:WK010     UDPARTREASON:TX010
        UDPARTSTAT:WK011        UDTHISSRC:TX011
        UDHIREDT:WK012          UDTRXPARTSTAT:TX012
        UDTERMDT:WK013          UDTRXELIGDATE:TX013
        UDCURRPS100:WK014       UDCTELIG:TX014
        UDCURRPS110:WK015       UDTRXTRLR:TX015
        UDPROJELIG:WK016        UDTRXDEVALUE:TX016
        UDNEWENTRYDT:WK017      UDCLIPUPDTDE:TX017
        UDTRXDENUM:WK018
       );
UDNEWENTRYDT = 99999999;
UDTRXTRLR = "";           /* Initialize T801 trailer id */
OCCLIP_CLEAR();
PTPHOBJ_GET(PLAN:SD001 PARTID:SD002);
UDCURRENTRYDT = PTPHOBJ_NUMDE(054);  /* plan entry date */
UDTERMDT = PTPHOBJ_NUMDE(056);       /* term date */
UDPARTSTAT = PTPHOBJ_NUMDE(021);     /* part status */
UDHIREDT = PTPHOBJ_NUMDE(052);       /* hire date */
UDPARTREASON = PTPHOBJ_DE(138);      /* part reason */
IF ((UDPARTSTAT <= 29) OR (UDTERMDT = 0))
   AND (UDPARTREASON <> "E") AND (UDPARTREASON <> "F");   /* CC10494: check active participants only */
  SOSCOBJ_VIEW(PLAN:SD001);
  LOOP WHILE SOSCOBJ_NEXT();
    UDTHISSRC = SOSCOBJ_DE(007);
    IF PTPSOBJ_GET(PLAN:SD001 PARTID:SD002 SOURCE:UDTHISSRC) = 0;    /* add any missing sources */
      PTPSOBJ_INIT(PLAN:SD001 PARTID:SD002 SOURCE:UDTHISSRC);
      PTPSOBJ_ADD();
    END;
    UDCURRPS100 = PTPSOBJ_NUMDE(100);
    UDPROJELIG = UDCURRPS100;  /* CC10542 initialize UDPROJELIG */
    UDCURRPS110 = PTPSOBJ_NUMDE(110);
** If running "FIXELIG" process or PS dates not set or future dated, recalc source entry dates **
    IF (UDRUNTYPE = "FIXELIG")
        OR (UDCURRPS100 = 0) OR (UDCURRPS100 > UDTRADEDT)
        OR (UDCURRPS110 = 0) OR (UDCURRPS110 > UDTRADEDT);
      UDPROJELIG = PLELIG_PROJECTENTRY(EFFDATE:UDTRADEDT SOURCE:UDTHISSRC);
      IF UDPROJELIG <> UDCURRPS110;
        OCCLIP_ADDLINE(("PS110,"+UDTHISSRC+","+OCFMT_DATE3(UDPROJELIG)));  /* 10494: add to clip instead of cards */
      END;
      IF UDPROJELIG <> UDCURRPS100;          /* If new PS100 date calculated */
        IF UDPROJELIG <= UDTRADEDT;          /* Reset PS100 to calculated date, if not in the future */
          OCCLIP_ADDLINE(("PS100,"+UDTHISSRC+","+OCFMT_DATE3(UDPROJELIG)));  /* 10494: add to clip instead of cards */
        ELSE;                /* If PS100 not already zero and new date is in the future, set PS100 to zero */
          IF UDCURRPS100 <> 0;
            OCCLIP_ADDLINE(("PS100,"+UDTHISSRC+","+OCFMT_DATE3(0)));  /* 10494: add to clip instead of cards */
          END;
        END;
      END;
    END;  /* CC10542  END added */
** Check if source updates plan entry and store earliest of those dates **
    PLELIG_VIEW(SOURCE:UDTHISSRC);
    UDCTELIG = PLELIG_CTLVAL(KEYWORD:"UPDTPLANENTRYDATE");
    IF (UDCTELIG = "0") OR (UDCTELIG="YES");       /* cc10850 - CC6823 If the current source updates plan entry dt */
      IF (UDPROJELIG < UDNEWENTRYDT) AND (UDPROJELIG <> 0);
        UDNEWENTRYDT = UDPROJELIG;                              /* store earliest date for PH054 update */
      END;
    END;
  ENDLOOP;
** If new PS dates calculated, may need to update PH054 and PH021
  IF OCCLIP_LENG() > 0;
    IF (UDNEWENTRYDT > 0) AND (UDNEWENTRYDT <= UDTRADEDT);
      IF UDPARTSTAT = 03;    /* CC6823 If PH054 date not in the future, set PH054 & PH021 */
        UDTRXPARTSTAT = "00";
        OCCLIP_INSLINEBEFORE(("PH021,"+UDTRXPARTSTAT) POS:1);
      END;
      IF UDNEWENTRYDT <> UDCURRENTRYDT;   /* If new elig date <> current plan entry date, add to clip */
        OCCLIP_INSLINEBEFORE(("PH054,"+OCFMT_DATE3(UDNEWENTRYDT)) POS:1);
        IF UDCURRENTRYDT > 0;  /* If PH054 valued and new entry date calculated, delete SV rec for current value */
           SVSVOBJ_VIEW(PLAN:SD001 PARTID:SD002 EFFECTDATE:UDCURRENTRYDT);
           LOOP WHILE SVSVOBJ_NEXT();
              BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"877" DATE:UDHIREDT);
              BATIUT_SETDE(050 UDCURRENTRYDT);
              BATIUT_SETDE(055 SVSVOBJ_NUMDE(012));
              BATIUT_SETDE(065 "2");
              BATIUT_SETVTDE(450 UDTRADEDT);   /* WMS1519B - set origination date to avoid rejects */
              BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
              BATIUT_ADDTRAN();
              BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S877") STEP:"PT"); /* must do in PT step */
           ENDLOOP;
        END;
      END;
    ELSEIF (UDNEWENTRYDT = 0) OR (UDNEWENTRYDT > UDTRADEDT);
      IF UDPARTSTAT = 00;
        UDTRXPARTSTAT = "03";
        OCCLIP_INSLINEBEFORE(("PH021,"+UDTRXPARTSTAT) POS:1);
        SVSVOBJ_VIEW(PLAN:SD001 PARTID:SD002 EFFECTDATE:UDCURRENTRYDT);
        LOOP WHILE SVSVOBJ_NEXT();
           BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"877" DATE:UDHIREDT);
           BATIUT_SETDE(050 UDCURRENTRYDT);
           BATIUT_SETDE(055 SVSVOBJ_NUMDE(012));
           BATIUT_SETDE(065 "2");
           BATIUT_SETVTDE(450 UDTRADEDT);   /* WMS1519B - set origination date to avoid rejects */
           BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
           BATIUT_ADDTRAN();
           BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S877") STEP:"PT");   /* must do in PT step */
        ENDLOOP;
      END;
      IF UDNEWENTRYDT <> UDCURRENTRYDT; /* otherwise directly update PH054=0 so no 0 SV rec is created */
        PTPHOBJ_SETDE(054 VALUE:00000000);
        PTPHOBJ_UPDATE();
      END;
    END;
** Create T801 transaction to update PH & PS DEsfrom clipboard data
    BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"801" DATE:UDHIREDT);
     BATIUT_SETDE(050 "CHG-PART");
     BATIUT_SETVTDE(450 UDTRADEDT);   /* set origination date to avoid rejects */
    BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
    BATIUT_ADDTRAN();          /* All T801 DEs on trailers, so add main trx first */
     UDTRXDENUM = 064;
     OCCLIP_VIEW();
     LOOP WHILE OCCLIP_NEXT();
       OCCSV_SETLINE(OCCLIP_LINE()); OCCSV_VIEW();
       UDCLIPUPDTDE = OCCSV_FIELD(1);
       IF (UDCLIPUPDTDE = "PH021") OR (UDCLIPUPDTDE = "PH054");  /* PH021 & 054 on PTEN trlr */
         IF UDTRXTRLR <> "PTEN";
           UDTRXTRLR = "PTEN";
         END;
         UDTRXDEVALUE = OCTEXT_SUB(UDCLIPUPDTDE 3 3);   /* DE to update in 64 & 69 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 1;
         BATIUT_SETDE(UDTRXDENUM (OCCSV_FIELD(2)) TRLR:UDTRXTRLR);  /* New DE value in 65 & 70 */
         UDTRXDENUM =+ 4;
       ELSE;                     /* PS100 & 110 on PTSO trailer */
         IF UDTRXTRLR <> "PTSO";
           IF UDTRXTRLR = "PTEN";
             BATIUT_ADDTRLR();   /* add PTEN trailer before starting PTSO updates */
           END;
           UDTRXDENUM = 60;
           UDTRXTRLR = "PTSO";
         END;
         IF UDTRXDENUM > 354;
            BATIUT_ADDTRLR();
            BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S801") STEP:"PT");
            BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"801" DATE:UDHIREDT);
            BATIUT_SETDE(050 "CHG-PART");
            BATIUT_SETVTDE(450 UDTRADEDT);   /* set origination date to avoid rejects */
            BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
            BATIUT_ADDTRAN();          /* All T801 DEs on trailers, so add main trx first */
            UDTRXDENUM = 60;
            UDTRXTRLR = "PTSO";
         END;
         UDTRXDEVALUE = OCCSV_FIELD(2);                /* Source in DEs 60-340 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 5;
         BATIUT_SETDE(UDTRXDENUM "O" TRLR:UDTRXTRLR);  /* Use override calc option in 65-345 */
         UDTRXDENUM =+ 5;
         UDTRXDEVALUE = OCTEXT_SUB(UDCLIPUPDTDE 3 3);  /* DE to update in 70-350 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 5;
         UDTRXDEVALUE = OCCSV_FIELD(3);                /* New DE value in 75-355 */
         BATIUT_SETDE(UDTRXDENUM UDTRXDEVALUE TRLR:UDTRXTRLR);
         UDTRXDENUM =+ 5;
       END;
     ENDLOOP;
     BATIUT_ADDTRLR();
    BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S801") STEP:"PT");
  ELSE;
    RPLOG_INFO(PLAN:SD001 PARTID:SD002 MSG:"AUL: No eligiblity changes made for participant");
  END;
ELSE;
  RPLOG_INFO(PLAN:SD001 PARTID:SD002 MSG:"AUL: Eligibility check skipped for participant");
END;
** Create T966 for IND-PRIORVEST
BATIUT_INIT(PLAN:SD001 PARTID:SD002 TRAN:"966" DATE:UDTRADEDT FUND:"***");
 BATIUT_SETDE(050 "IND-PRIORVEST");
 BATIUT_SETVTDE(775 ("IND-"+UDRUNTYPE));
BATIUT_ADDTRAN();
BATIUT_TONEXTTI(FILEID:(OCFMT_DATE3(UDTRADEDT)+".S966") STEP:"PT");
@  ���G���PJ  U P6WAB P  �Z   T  W 
 H VST-CHANGEELIG I Initial Load
X >Yx|S 	Baseline    S change to v3.0    ^ Production    @  ���f���o